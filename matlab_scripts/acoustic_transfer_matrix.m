function G = acoustic_transfer_matrix(S_f, c, pos_l, pos_c, plane_wave)
% acoustic_transfer_matrix - computes acoustic transfer matrix from a set
%                            of loudspeakers whose locations are stored in
%                            pos_l to a set of control points whose locations
%                            are stored in pos_c at frequency S_f
%
% Inputs
%  S_f:        temporal frequency [Hz]
%  c:          speed of sound [m/s]
%  pos_l:      if plane_wave == 1,  pos_l(1) contains azimuth angle of
%                                   plane wave propagation,
%                                   pos_l(2) contains elevation angle
%              if plane_wave =/= 1, pos_l is an array containing
%                                   locations of
%                                   loudspeakers (default 2D) Nx2
%  pos_c:      array containing locations of control points (default 2D)
%              Nx2
%  plane_wave: non-zero value indicates that a propagating wave is a
%              plane wave
%
% Outputs
%  G:          transfer matrix (propagation matrix)

% Author:      Mihailo Kolundzija (mihailo.kolundzija@epfl.ch)
% Date:        April 2011


N_l = size(pos_l,1);
N_c = size(pos_c,1);
k = 2*pi*S_f/c;

% spherical waves (point sources)
if nargin == 4 || isempty(plane_wave) || plane_wave == 0

    r = zeros(N_c,N_l);
    for i = 1:size(pos_l,2)
        M = ones(N_c,1) * pos_l(:,i)';
        N = pos_c(:,i) * ones(1,N_l);
        r = r + (M-N).^2;
    end
    r = sqrt(r);
    % propagation characteristic from loudspeakers to control points
    G = 1./(4*pi*r) .* exp(-1j*k*r);

else % plane wave propagation

    if size(pos_l,1)*size(pos_l,2) > 2
        disp(['pos_l should contain plane wave propagation angle' ...
             '(azimuth and possibly elevation).']);
        return;
    end

    theta = pos_l(1); % azimuth

    % take elevation
    if length(pos_l) > 1
        phi = pos_l(2); % elevation
    else
        phi = 0;
    end

    G = exp(-1j*k*(pos_c(:,1).*cos(theta).*cos(phi) + ...
            pos_c(:,2).*sin(theta).*cos(phi)));

end
