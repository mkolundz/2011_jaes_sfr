function compute_filters(src_pos, ...
                         src_plane_wave, ...
                         ldspk_select, ...
                         filters_filename)
% compute_filters - computes SFR and WFS filters for different point
%   sources determined by the chosen experiment. Setup is fixed, results
%   are written to files.
%
% Inputs
%   src_pos:           matrix with x and y coordinates of primary sources [m]
%   src_plane_wave:    vector with 1 for sources which are plane waves
%   ldspk_select:      vector with 1 for sources reproduced with a subset
%                      of loudspeakers
%   filters_filename:  name of the file where parameters and filters are stored
%
% Note:
%   A number of important parameters are initialized through the file
%   experiment_parameters.m. This solution is not ideal, but avoids using
%   a function with a large number of parameters or an equally sloppy way
%   of passing a very complex structure with all the parameters.
%   Additionally, filter coefficients and additional delays are stored into
%   files which are later used by scripts, again for the same practical
%   reasons.

% Author:              Mihailo Kolundzija (mihailo.kolundzija@epfl.ch)
% Date:                April 2011

% load all the experiment parameters
experiment_parameters;

% arrays that will store loudspeaker filters and delays for all source
% positions
sfr_filters_array = zeros(size(src_pos,1), N_l, filter_size);
wfs_filters_s_array = zeros(size(src_pos,1), N_l, filter_size);
wfs_filters_a_array = zeros(size(src_pos,1), N_l, filter_size);
sfr_delays_array = zeros(size(src_pos,1), N_l);
wfs_delays_s_array = zeros(size(src_pos,1), N_l);
wfs_delays_a_array = zeros(size(src_pos,1), N_l);
max_neg_delays_array = zeros(size(src_pos,1),1);
wfs_speakers_s_array = zeros(size(src_pos,1), N_l);
wfs_speakers_a_array = zeros(size(src_pos,1), N_l);
sfr_speakers_array = zeros(size(src_pos,1), N_l);

t_start = tic;

for src_num = 1:size(src_pos,1)

  t_iteration_start = tic;

  % primary source position (or plane wave angle)
  r_m = src_pos(src_num, :);
  % plane wave indicator
  plane_wave = src_plane_wave(src_num);

  % move the control points forward if the primary source is too close
  if abs(r_m(1)-r_c(1)) < min_ctrl_grid_dist && plane_wave == 0
    r_c(1) = r_m(1) + min_ctrl_grid_dist;
    disp('Control points moved forward');
  end
  % move the control points forward if the loudspeakers are too close
  if (abs(x_l-r_c(1)) < min_ctrl_grid_dist)
    r_c(1) = x_l + min_ctrl_grid_dist;
    disp('Control points moved forward');
  end

  % determine positions of USED loudspeakers and their indexes and
  % USED control points
  % compute reference points' coordinates
  pos_c = make_ctrl_point_grid(r_m, r_c, plane_wave, ...
    ctrl_grid_bounds, delta_c, ref_points_shape);
  % determine the subset of used loudspeakers and control points
  if ldspk_select(src_num)
    % SFR setup
    [pos_sfr, pos_c_sfr, sfr_idxs] = ...
      spk_ctrl_select(r_m, plane_wave, pos_l, pos_c, delta_l, 10*delta_c);
    % WFS setup with a subset of loudspeakers used for SFR
    [pos_wfs_s, ~, wfs_s_idxs] = ...
      spk_ctrl_select(r_m, plane_wave, pos_l, pos_c, delta_l, 10*delta_c);
    %   plane_wave, pos_l, ref_line, delta_l, 10*delta_c);
  else
    pos_sfr = pos_l;
    pos_c_sfr = pos_c;
    sfr_idxs = 1:size(pos_sfr, 1);
    % WFS setup with a subset of loudspeakers used for SFR
    pos_wfs_s = pos_sfr;
    wfs_s_idxs = sfr_idxs;
  end
  % WFS setup with all loudspeakers
  pos_wfs_a = pos_l;
  wfs_a_idxs = 1:size(pos_l,1);

  % store the number of loudspeakers for different setups
  % number of loudspeakers for WFS with a subset of spks
  N_wfs_s = size(pos_wfs_s, 1);
  % number of loudspeakers for WFS with all spks
  N_wfs_a = size(pos_wfs_a, 1);
  % number of loudspeakers for SFR
  N_sfr = size(pos_sfr, 1);

  wfs_win_trans_len_a = round(N_wfs_a * wfs_win_taper_len);
  wfs_win_trans_len_s = round(N_wfs_s * wfs_win_taper_len);

  % make a tapering window for WFS using all loudspeakers
  wfs_win_a = tapering_window(pos_wfs_a, wfs_win_type, ...
                              wfs_win_trans_len_a);
  % make a tapering window for WFS using a subset of loudspeakers
  wfs_win_s = tapering_window(pos_wfs_s, wfs_win_type, ...
                              wfs_win_trans_len_s);


  % compute WFS and SFR filters (time domain filters)
  % -------------------------------------------------

  % compute WFS filters with a subset of speakers
  [wfs_filters_a wfs_delays_a neg_delay_wfs_a] = ...
    wfs_filters_t(fft_size, filter_size, f_s, c, delta_l, pos_wfs_a, ...
                  ref_line, r_m, plane_wave, wfs_win_a, norm_gains_wfs, ...
                  lp_filter_f, prun_win);

  % compute WFS filters with all speakers
  [wfs_filters_s wfs_delays_s neg_delay_wfs_s] = ...
    wfs_filters_t(fft_size, filter_size, f_s, c, delta_l, pos_wfs_s, ...
                  ref_line, r_m, plane_wave, wfs_win_s, norm_gains_wfs, ...
                  lp_filter_f, prun_win);

  % compute SFR filters
  [sfr_filters sfr_delays neg_delay_sfr] = ...
    sfr_filters_t(fft_size, filter_size, f_s, c, pos_sfr, pos_c_sfr, ...
                  ref_line, r_m, plane_wave, pinv_condnums, norm_gains_sfr, ...
                  smooth_len, lp_filter_f, prun_win);

  % find the maximum negative delay
  max_neg_delay = max(max(neg_delay_wfs_a, neg_delay_wfs_s), ...
                      neg_delay_sfr);

  wfs_delays_a = wfs_delays_a + (max_neg_delay-neg_delay_wfs_a);
  wfs_delays_s = wfs_delays_s + (max_neg_delay-neg_delay_wfs_s);
  sfr_delays = sfr_delays + (max_neg_delay-neg_delay_sfr);

  if (sum(isnan(wfs_filters_a))) > 0
    disp('WFS filters have NaN entries');
    return;
  end

  wfs_speakers_a = zeros(1, N_l);
  wfs_speakers_s = zeros(1, N_l);
  sfr_speakers = zeros(1, N_l);
  wfs_speakers_a(wfs_a_idxs) = 1;
  wfs_speakers_s(wfs_s_idxs) = 1;
  sfr_speakers(sfr_idxs) = 1;

  sfr_filters_array(src_num, sfr_idxs, :) = sfr_filters;
  sfr_delays_array(src_num, sfr_idxs) = sfr_delays;
  wfs_filters_s_array(src_num, wfs_s_idxs, :) = wfs_filters_s;
  wfs_delays_s_array(src_num, wfs_s_idxs) = wfs_delays_s;
  wfs_filters_a_array(src_num, 1:N_l, :) = wfs_filters_a;
  wfs_delays_a_array(src_num, 1:N_l) = wfs_delays_a;
  max_neg_delays_array(src_num) = max_neg_delay;
  wfs_speakers_s_array(src_num, :) = wfs_speakers_s;
  wfs_speakers_a_array(src_num, :) = wfs_speakers_a;
  sfr_speakers_array(src_num, :) = sfr_speakers;

  t_iteration = toc(t_iteration_start);

  disp(['Filters for source ' num2str(src_num) ' computed in ' ...
       num2str(t_iteration) ' seconds']);

end

t_end = toc(t_start);
disp(['Filters computed in ', num2str(t_end), ' seconds']);

% save computed filters and delays to a file, together with setup
% parameters (primary source, loudspeakers, and control points
% positions), sampling frequency, filter lengths, FFT length

save(filters_filename, ...
     'sfr_filters_array', 'sfr_delays_array', ...
     'wfs_filters_s_array', 'wfs_delays_s_array', ...
     'wfs_filters_a_array', 'wfs_delays_a_array', ...
     'wfs_speakers_s_array', 'wfs_speakers_a_array', ...
     'sfr_speakers_array', 'max_neg_delays_array', ...
     'src_pos', 'src_plane_wave');
