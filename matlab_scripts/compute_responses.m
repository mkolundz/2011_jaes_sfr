function compute_responses(dx, dy, startx, filters_filename, response_filename)
% compute_responses - computes responses for WFS I, WFS II and SFR
%                     precomputed filters on a defined grid of listening points
%
% Inputs
%   dx:                distance between listening locations along x axis [m]
%   dy:                distance between listening locations along y axis [m]
%   startx:            x coordinate of the first line of listening points [m]
%   filters_filename:  file where parameters and filters are stored
%   response_filename: file where parameters and responses are stored
%
% Note:
%   A number of important parameters are initialized through the file
%   experiment_parameters.m. This solution is not ideal, but avoids using
%   a function with a large number of parameters or an equally sloppy way
%   of passing a very complex structure with all the parameters.

% Author:              Mihailo Kolundzija (mihailo.kolundzija@epfl.ch)
% Date:                April 2011

% load all the experiment parameters
experiment_parameters;

load(filters_filename, ...
     'sfr_filters_array', 'sfr_delays_array', ...
     'wfs_filters_s_array', 'wfs_delays_s_array', ...
     'wfs_filters_a_array', 'wfs_delays_a_array', ...
     'max_neg_delays_array', ...
     'src_pos', 'src_plane_wave');

% determine listening point positions
pos_y = (0:dy:room_y)';
pos_x = (startx:dx:room_x)';
N_x = length(pos_x);
N_src = size(src_pos,1) / 3;
% listening locations
pos_c = [kron(pos_x,ones(length(pos_y),1)) kron(ones(length(pos_x),1), pos_y)];
N_c = size(pos_c,1); % number of listening locations

% stimulus will contain the excitation signal

% plot frequency responses, group delays, and impulse responses in the
% listening points. Save them as jpg files
stimulus = make_stimulus(f_s, 0.5, 5);

% define frequency grid to compute group delays and spectra
grp_del_freqs = (0:(fft_size/2))/fft_size*f_s;
grp_del_freqs = ...
  grp_del_freqs(find((grp_del_freqs>50) & (grp_del_freqs<18000)));

% initialize to zero group delay and spectrum arrays
desired_grp_delay = zeros(N_src*N_c, length(grp_del_freqs));
sfr_grp_delay = zeros(N_src*N_c, length(grp_del_freqs));
wfs_a_grp_delay = zeros(N_src*N_c, length(grp_del_freqs));
wfs_s_grp_delay = zeros(N_src*N_c, length(grp_del_freqs));

desired_spectrum = zeros(N_src*N_c, length(grp_del_freqs));
sfr_spectrum = zeros(N_src*N_c, length(grp_del_freqs));
wfs_a_spectrum = zeros(N_src*N_c, length(grp_del_freqs));
wfs_s_spectrum = zeros(N_src*N_c, length(grp_del_freqs));

t_start = tic; % start measuring loop execution time(s)

for m = 1:3*N_src

    t_start_iteration = tic; % measure current loop's time

    r_m = src_pos(m, :);
    plane_wave = src_plane_wave(m);
    wfs_filters_a = squeeze(wfs_filters_a_array(m,:,:));
    wfs_filters_s = squeeze(wfs_filters_s_array(m,:,:));
    sfr_filters = squeeze(sfr_filters_array(m,:,:));
    wfs_delays_a = squeeze(wfs_delays_a_array(m,:));
    wfs_delays_s = squeeze(wfs_delays_s_array(m,:));
    sfr_delays = squeeze(sfr_delays_array(m,:));
    max_neg_delay = max_neg_delays_array(m);

    [desired_response] = ...
      snd_field_responses(f_s, c, r_m, pos_c, ...
                          plane_wave, stimulus, [1 0], ...
                          max_neg_delay+filter_size/2/f_s, 0);
    [wfs_response_a] = ...
      snd_field_responses(f_s, c, pos_l, pos_c, ...
                          0, stimulus, wfs_filters_a, wfs_delays_a, 0);
    [wfs_response_s] = ...
      snd_field_responses(f_s, c, pos_l, pos_c, ...
                          0, stimulus, wfs_filters_s, wfs_delays_s, 0);
    [sfr_response] = ...
      snd_field_responses(f_s, c, pos_l, pos_c, ...
                          0, stimulus, sfr_filters, sfr_delays, 0);

    % define start and end points of responses in each point to have a
    % shorter segment for computing group delays and spectra
    impulse_start_delay = min(min(wfs_delays_a(:)), min(sfr_delays(:))) ...
                        + abs(pos_l(1,1) - min(pos_c(:,1)))/c;
    impulse_start_idx = round(impulse_start_delay * f_s);
    impulse_end_delay = max(max(sfr_delays(:)), max(wfs_delays_a(:))) ...
                      + norm(pos_l(1,:) - pos_c(end,:))/c;
    impulse_end_idx = round(impulse_end_delay * f_s) + filter_size;

    for i = 1:N_c
        j = (m-1)*N_c+i;
        % compute group delays
        desired_grp_delay(j,:) = ...
          grpdelay(desired_response(i, ...
                   impulse_start_idx:impulse_end_idx), 1, grp_del_freqs, f_s);
        sfr_grp_delay(j,:) = ...
          grpdelay(sfr_response(i, ...
                   impulse_start_idx:impulse_end_idx), 1, grp_del_freqs, f_s);
        wfs_a_grp_delay(j,:) = ...
          grpdelay(wfs_response_a(i, ...
                   impulse_start_idx:impulse_end_idx), 1, grp_del_freqs, f_s);
        wfs_s_grp_delay(j,:) = ...
        grpdelay(wfs_response_s(i, ...
                 impulse_start_idx:impulse_end_idx), 1, grp_del_freqs, f_s);

        % compute spectra
        desired_spectrum(j,:) = ...
          abs(freqz(desired_response(i, ...
              impulse_start_idx:impulse_end_idx), 1, grp_del_freqs, f_s));
        sfr_spectrum(j,:) = ...
          abs(freqz(sfr_response(i, ...
              impulse_start_idx:impulse_end_idx), 1, grp_del_freqs, f_s));
        wfs_a_spectrum(j,:) = ...
          abs(freqz(wfs_response_a(i, ...
              impulse_start_idx:impulse_end_idx), 1, grp_del_freqs, f_s));
        wfs_s_spectrum(j,:) = ...
          abs(freqz(wfs_response_s(i, ...
              impulse_start_idx:impulse_end_idx), 1, grp_del_freqs, f_s));

        if sum(desired_spectrum(j,:)) == 0
            disp('Desired spectrum equal to zero!');
            return;
        end
    end

    t_end_iteration = toc(t_start_iteration);
    disp(['Processed source ' num2str(m) ' in ' ...
      num2str(t_end_iteration) ' seconds']);

end

% display accumulated execution time
t_end = toc(t_start);
disp(['Finished processing in ', num2str(t_end), ' seconds']);

% adjust the group delays relative to the desired group delay
wfs_a_grp_delay_err = (wfs_a_grp_delay-desired_grp_delay);
wfs_s_grp_delay_err = (wfs_s_grp_delay-desired_grp_delay);
sfr_grp_delay_err = (sfr_grp_delay-desired_grp_delay);

% normalize spectra by the desired spectrum
sfr_spectrum_norm = (sfr_spectrum ./ desired_spectrum);
wfs_a_spectrum_norm = (wfs_a_spectrum ./ desired_spectrum);
wfs_s_spectrum_norm = (wfs_s_spectrum ./ desired_spectrum);

% save responses and parameters to a file
save(response_filename, ...
     'wfs_a_grp_delay_err', 'wfs_s_grp_delay_err', 'sfr_grp_delay_err', ...
     'sfr_spectrum_norm', 'wfs_a_spectrum_norm', 'wfs_s_spectrum_norm', ...
     'desired_grp_delay', 'sfr_grp_delay', ...
     'wfs_a_grp_delay', 'wfs_s_grp_delay', ...
     'desired_spectrum', 'sfr_spectrum', ...
     'wfs_a_spectrum', 'wfs_s_spectrum', ...
     'grp_del_freqs', 'pos_c', 'f_s', 'N_x', 'N_src');
