% Physical constants
c = 340; % speed of sound [m/s]

%-------------------%
% Setup coordinates %
%------------------ %

room_x =              10; % size of listening area along x axis [m]
room_y =               4; % size of listening area along y axis [m]
delta  =            0.04; % spatial sampling interval in y [m]
                          % (used for plotting snapshots)
delta_l =            .15; % loudspeaker spacing [m]
N_l =                 18; % number of loudspeakers
delta_c =          0.015; % control point spacing [m]
ctrlpts_offset_down =  0; % control points array extension beyond room
                          % boundaries along negative y [m]
ctrlpts_offset_up =    0; % control points array extension beyond room
                          % boundaries along positive y [m]
listening_width =      3; % width of the listening area around the
                          % listener line [m]
x_l =                4.0; % x coordinate of loudspeaker array [m]
r_c =     [8.0 room_y/2]; % listening position (listening line at r_c(1))
x_ref =                8; % x coordinate of reference
                          % (used for power normalization) line [m]
min_ctrl_grid_dist = 2.5; % minimum distance from any source to the
                          % control grid (avoids over-fitting) [m]

% control points grid boundaries
contr_min_y =        ctrlpts_offset_down; % lower margin
contr_max_y = room_y + ctrlpts_offset_up; % upper margin
contr_min_x = r_c(1) - listening_width/2; % left margin
contr_max_x = r_c(1) + listening_width/2; % right margin
ctrl_grid_bounds = [contr_min_x contr_max_x contr_min_y contr_max_y];


%-------------------------------%
% filter computation parameters %
%-------------------------------%

% Time- and frequency-domain processing parameters
f_s =              48000; % temporal sampling frequency [Hz]
fft_size =          2048; % size of the fft used for filter design
filter_size =        512; % size (in samples) of SFR and WFS filters
smooth_len =          13; % length of smoothing filter (should be odd)
lp_filter_size =      64; % length of the low pass filter for cutting off
                          % frequencies above 20 kHz
                          % (will be one tap longer)

prun_window_type =     3; % 1: rectangular
                          % 2: sine window
                          % 3: rectangular with a cosine tapering

% taper width (in samples) of the time-domain window used for extracting
% the loudspeaker filters after computing them in the frequency domain
trans_band_size = filter_size/8;

% numerical parameters
pinv_condnums =      10; % maximum allowed condition number for pseudoinverse

% percentage of the WFS loudspeaker array that is tapered
wfs_win_taper_len =  0.15;

% shape of the reference point surface
ref_points_shape = 3; %  1: vertical line
                      %  2: cross
                      %  3: rectangular grid
                      %  4: line orthogonal to the line from the
                      %     source to the reference point
                      %  5: arc centered in r_m with a radius |r_m-r_c|
                      %  6: concentric arcs centered in r_m

% spatial window for WFS loudspeaker array
wfs_win_type =     3; % 1: rectangular loudspeaker window
                      % 2: sine loudspeaker window
                      % 3: tapering window of length defined by
                      %    wfs_win_trans_len

% normalization type for WFS filters
norm_gains_wfs =   1; % 0: use gains from optimization procedure
                      % 1: fix loudspeaker gains to have desired average
                      %    gains on the reference line
                      % 2: fix loudspeaker gains to have desired gain in
                      %    the listening point

% normalization type for SFR filters
norm_gains_sfr =   1; % 0: use gains from optimization procedure
                      % 1: fix loudspeaker gains to have desired average
                      %    gains on the reference line
                      % 2: fix loudspeaker gains to have desired gain in
                      %    the listening point

%--------------%
% Computations %
%--------------%

% determine positions of loudspeakers
ldspk_array_start = room_y/2-(N_l-1)/2*delta_l;
yi = ldspk_array_start:delta_l:ldspk_array_start+(N_l-1)*delta_l;
pos_l = zeros(length(yi),2);
pos_l(:,2) = yi;
pos_l(:,1) = x_l;

% determine the reference line for spatial averaging of the loudspeaker
% responses
ref_line(:,2) = contr_min_y:delta_c:contr_max_y;
ref_line(:,1) = x_ref;

% compute a low pass filter when sampling frequency is above 40 kHz
if f_s/2 > 20000
  % apply low-pass filter in order to have loudspeaker filters with 20kHz
  % cut-off frequency
  lp_filter_t = firpm(lp_filter_size, ...
                [0 18000.0/(f_s/2) 20000.0/(f_s/2) 1.0], [1 1 0 0]);
  lp_filter_f = fft(lp_filter_t, fft_size);
  % make frequency response zero-phase in order to avoid additional
  % delays to the loudspeaker filters
  lp_filter_f = lp_filter_f .* ...
                exp(1j*2*pi*(0:fft_size-1)/fft_size*(lp_filter_size)/2);
else
  lp_filter_f = ones(1, fft_size);
end

% compute the pruning window for shortening the loudspeaker filters
prun_win = pruning_window(fft_size, filter_size, ...
                          prun_window_type, trans_band_size);
