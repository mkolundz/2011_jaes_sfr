function [pos_c N_c] = make_ctrl_point_grid(r_m, ...
                                            r_c, ...
                                            plane_wave, ...
                                            grid_bounds, ...
                                            delta_c, ...
                                            grid_shape)
% make_ctrl_point_grid - creates a grid of control points.
%
% Input parameters
%  r_m:         if plane_wave == 0,  r_m contains location of the primary
%                                    source (default 2D) 1x2
%               if plane_wave =/= 0, r_m(1) contains azimuth angle of
%                                    plane wave propagation;
%                                    r_m(2) contains elevation
%  r_c:         location of the reference listening point
%  plane_wave:  non-zero value indicates that a propagating wave is a
%  grid_bounds: bounds (min_x max_x min_y max_y) for the reference grid
%  delta_c:     distance between points in the reference grid
%  grid_shape:  shape of the control point grid
%                 1: vertical line (default)
%                 2: cross
%                 3: rectangular grid
%                 4: line orthogonal to the vector (r_c-r_m)
%                 5: arc centered in r_m with a radius |r_m-r_c|
%                 6: concentric arcs centered in r_m
%
% Outputs
%  ctrl_pts:    locations of control points
%  N_c:         number of control points

% Author:       Mihailo Kolundzija (mihailo.kolundzija@epfl.ch)
% Date:         April 2011

contr_min_x = grid_bounds(1);
contr_max_x = grid_bounds(2);
contr_min_y = grid_bounds(3);
contr_max_y = grid_bounds(4);


% compute reference points' coordinates
if grid_shape == 2 % reference cross
  ii = 0;
  for yy = contr_min_y:delta_c:contr_max_y
    ii = ii+1;
    pos_c(ii,1) = r_c(1);
    pos_c(ii,2) = yy;
  end
  for xx = contr_min_x:delta_c:contr_max_x
    ii = ii + 1;
    pos_c(ii,1) = xx;
    pos_c(ii,2) = r_c(2);
  end
  N_c = ii;
elseif grid_shape == 3 % reference grid
  ii = 0;
  for xx = contr_min_x:50*delta_c:contr_max_x
    for yy = contr_min_y:delta_c:contr_max_y
      ii = ii+1;
      pos_c(ii,1) = xx;
      pos_c(ii,2) = yy;
    end
  end
  N_c = ii;
elseif grid_shape == 4 % reference line orthogonal to the vector (r_c-r_m)
  if plane_wave == 0
    r = r_c - r_m; % vector pointing to r_c from r_m
    r = r ./ norm(r); % normalize r
  else
    r = [cos(r_m(1))*cos(r_m(2)) sin(r_m(1))*cos(r_m(2))];
    r = r ./ norm(r);
  end
  r_orth = [-r(2) r(1)]; % find the vector orthogonal to r
  r_orth = r_orth ./ norm(r_orth); % normalize r_orth
  if r_orth(2) < 0
    r_orth = -r_orth; % make r_orth point to positive y direction
  end
  r = r_c; % r will contain coordinates of control points
  pos_c(1,:) = r;
  ii = 1;
  while (r(1) >= contr_min_x) && (r(1) <= contr_max_x) && ...
        (r(2) <= contr_max_y)
    ii = ii + 1;
    r = r + r_orth .* delta_c;
    pos_c(ii,:) = r;
  end
  r = r_c - r_orth .* delta_c;
  while (r(1) >= contr_min_x) && (r(1) <= contr_max_x) && ...
        (r(2) >= contr_min_y)
    ii = ii + 1;
    pos_c(ii,:) = r;
    r = r - r_orth .* delta_c;
  end
  N_c = ii;
elseif grid_shape == 5
  % reference arc centered at r_m with radius |r_m-r_c|
  if plane_wave == 0
    r = norm(r_m-r_c);
    ang_delta_c = delta_c / r;
    Rot = [cos(ang_delta_c) -sin(ang_delta_c); ...
           sin(ang_delta_c) cos(ang_delta_c)];
    pos_c(1,2) = contr_min_y;
    pos_c(1,1) = r_m(1) + sqrt(r^2 - (r_m(2)-contr_min_y)^2);
    r_rot = r_m + (pos_c - r_m) * Rot';
    ii = 1;
    while r_rot(2) <= contr_max_y
      ii = ii + 1;
      pos_c(ii,:) = r_rot;
      r_rot = r_m + (r_rot - r_m) * Rot';
    end
    N_c = ii;
  else % plane_wave == 1
    r = [cos(r_m(1))*cos(r_m(2)) sin(r_m(1))*cos(r_m(2))];
    r = r ./ norm(r);
    r_orth = [-r(2) r(1)]; % find the vector orthogonal to r
    r_orth = r_orth ./ norm(r_orth); % normalize r_orth
    if r_orth(2) < 0
      r_orth = -r_orth; % make r_orth point to positive y direction
    end
    r = r_c; % r will contain coordinates of control points
    pos_c(1,:) = r;
    ii = 1;
    while (r(1) >= contr_min_x) && (r(1) <= contr_max_x) && ...
          (r(2) <= contr_max_y)
      ii = ii + 1;
      r = r + r_orth .* delta_c;
      pos_c(ii,:) = r;
    end
    r = r_c - r_orth .* delta_c;
    while (r(1) >= contr_min_x) && (r(1) <= contr_max_x) && ...
          (r(2) >= contr_min_y)
      ii = ii + 1;
      pos_c(ii,:) = r;
      r = r - r_orth .* delta_c;
    end
    N_c = ii;
  end
elseif grid_shape == 6
  % cocentric arcs centered at r_m with radius |r_m-r_c|
  if plane_wave == 0
    ii = 0;
    for x = r_c(1):(50*delta_c):contr_max_x
      p_c(1,1) = x;
      p_c(1,2) = contr_min_y;
      r = norm(r_m-p_c);
      ang_delta_c = delta_c / r;
      Rot = [cos(ang_delta_c) -sin(ang_delta_c); ...
             sin(ang_delta_c) cos(ang_delta_c)];
      ii = ii + 1;
      pos_c(ii,:) = p_c;
      r_rot = r_m + (pos_c(ii,:) - r_m) * Rot';
      while r_rot(2) <= contr_max_y && r_rot(1) >= r_c(1)
        ii = ii + 1;
        pos_c(ii,:) = r_rot;
        r_rot = r_m + (r_rot - r_m) * Rot';
      end
    end
    N_c = ii;
  else
    r = [cos(r_m(1))*cos(r_m(2)) sin(r_m(1))*cos(r_m(2))];
    r = r ./ norm(r);
    r_orth = [-r(2) r(1)]; % find the vector orthogonal to r
    r_orth = r_orth ./ norm(r_orth); % normalize r_orth
    if r_orth(2) < 0
      r_orth = -r_orth; % make r_orth point to positive y direction
    end
    ii = 0;
    for x = r_c(1):(50*delta_c):contr_max_x
      r = [x r_c(2)];
      ii = ii + 1;
      pos_c(ii,:) = r;
      while (pos_c(ii,1) >= contr_min_x) && ...
          (pos_c(ii,1) <= contr_max_x) && (pos_c(ii,2) <= contr_max_y)
        ii = ii + 1;
        pos_c(ii,:) = pos_c(ii-1,:) + r_orth .* delta_c;
      end
      r_tmp = r - r_orth .* delta_c;
      while (r_tmp(1) >= contr_min_x) && (r_tmp(1) <= contr_max_x) && ...
            (r_tmp(2) >= contr_min_y)
        ii = ii + 1;
        pos_c(ii,:) = r_tmp;
        r_tmp = r_tmp - r_orth .* delta_c;
      end
    end
    N_c = ii;
  end
else % reference line (default)
  % if grid_shape == 1 (or any other value)% reference line
  ii = 0;
  for yy = contr_min_y:delta_c:contr_max_y
    ii = ii+1;
    pos_c(ii,1) = r_c(1);
    pos_c(ii,2) = yy;
  end
  N_c = ii;
end
