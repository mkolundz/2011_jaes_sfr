function stimulus = make_stimulus(f_s, ...
                                  stimulus_length, ...
                                  stimulus_type, ...
                                  stimulus_params)
% make_stimulus - makes a stimulus from a set of different stimuli defined
%                 by the parameters stimulus_type
%
% Inputs
%  f_s: sampling frequency [Hz]
%  stimulus_length: stimulus duration [s]
%  stimulus_type:   1: train of pulses
%                   2: pink noise
%                   3: sinusoid
%                   4: train of low-pass pulses
%                   5: single pulse (Dirac)
%  stimulus_params: array containing parameters of the desired stimulus
%                   stimulus_type == 1: stimulus_params(1) contains pulse
%                                       train period
%                   stimulus_type == 2: ignored
%                   stimulus_type == 3: stimulus_params(1) contains the
%                                       frequency of the sinusoid
%                   stimulus_type == 4: stimulus_params(1) contains pulse
%                                       train period
%                                       stimulus_params(2) contains
%                                       passband right margin
%                                       stimulus_params(3) contains
%                                       stopband left margin
%                   stimulus_type == 5: ignored
% Outputs
%  stimulus:        stimulus waveform

% Author:           Mihailo Kolundzija (mihailo.kolundzija@epfl.ch)
% Date:             April 2011

s = zeros(1, stimulus_length * f_s);
if stimulus_type == 1
    pulse_train_period = stimulus_params(1);
    train_period_samples = round(pulse_train_period * f_s);
    s(1:train_period_samples:end) = 1;
elseif stimulus_type == 2
    s = spatialPattern([1,stimulus_length*f_s],-1);
elseif stimulus_type == 3
    S_f = stimulus_params(1);
    tt = (0:length(s)-1)./f_s;
    s = sin(2*pi*S_f*tt);
elseif stimulus_type == 4
    pulse_train_period = stimulus_params(1);
    f_m = stimulus_params(2);
    f_m_r = stimulus_params(3);
    train_period_samples = round(pulse_train_period * f_s);
    s(1:train_period_samples:end) = 1;
    % make a low-pass filter to reduce the frequency contents of pink noise
    h = firpm(128, [0 f_m/f_s*2 f_m_r/f_s*2 1], [1 1 0 0]);
    s = filter(h,1,s);
elseif stimulus_type == 5
    s(1) = 1;
end

stimulus = s;
