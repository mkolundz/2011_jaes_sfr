function [] = plot_soundfield_snapshot(x_area, ...
                                       y_area, ...
                                       field_snapshot, ...
                                       scale, ...
                                       pos_l, ...
                                       r_m, ...
                                       pos_c, ...
                                       plane_wave, ...
                                       plot_title)
% plot_soundfield_snapshot - plots a snapshot of a sound field
%
% Inputs
%  x_area:         plotting grid along x axis
%  y_area:         plotting grid along y axis
%  field_snapshot: matrix containing the snapshot of the sound field
%  scale:          output values extremes relative to which the sound
%                  field is scaled
%  pos_l:          array containing locations of loudspeakers
%                  (default 2D) Nx2
%  r_m:            if plane_wave == 1, then r_m(1) contains azimuth angle
%                                      of plane wave propagation; r_m(2)
%                                      contains elevation
%                  if plane_wave =/=1, then r_m contains location of the
%                                      primary source
%  pos_c:          locations of control points
%  plane_wave:     =/= 0: the propagating wave is a plane wave
%  plot_title:     title to show on the plot

% Author:          Mihailo Kolundzija (mihailo.kolundzija@epfl.ch)
% Date:            April 2011

imagesc(x_area, y_area, field_snapshot, scale);
set(gca, 'ydir', 'normal');
xlabel('x [m]')
ylabel('y [m]')
if nargin == 9
  title(plot_title)
end
hold on
if ~isempty(pos_c)
  plot(pos_c(:,1), pos_c(:,2), 'k.', 'linewidth', .75, 'markersize', 3);
  text(pos_c(1,1), -.8,'x_c')
end
if ~isempty(pos_l)
  plot(pos_l(:,1), pos_l(:,2), 'ks', 'linewidth', 2);
  text(pos_l(1,1), -.8,'x_l')
end
if plane_wave == 0
  plot(r_m(1), r_m(2), 'ko', 'linewidth', 2);
end
hold off
axis image
