function w = pruning_window(fft_size, filter_size, ...
                            window_type, trans_band_size)
% pruning_window - computes a pruning window that is used to obtain a
%                  time-domain filter of a desired length from a long filter
%                  obtained by taking iDFT of the frequency-domain filter
%                  description
%
% Inputs
%  fft_size:        length of the used DFT
%  filter_size:     length of the desired filter
%  window_type:     type of the used pruning window
%                    1: rectangular window
%                    2: sine window
%                    3: tapering window with a defined transition band
%  trans_band_size: length of the taper on both sides of the pruning window
%                   used only in case window_type == 3
%
% Outputs
%  w:               pruning window to be used for obtaining the desired
%                   filter

% Author:           Mihailo Kolundzija (mihailo.kolundzija@epfl.ch)
% Date:             April 2011

% compute the window
x = 0:filter_size-1;
if window_type == 1
    window = ones(1,filter_size);
elseif window_type == 2
    window = sin(x./filter_size.*pi);
elseif window_type == 3
    xx = 0:trans_band_size-1;
    trans_band = 1/2 - 1/2 .* cos(xx./(trans_band_size+1)*pi);
    window = [trans_band ones(1,filter_size-2*trans_band_size) ...
              fliplr(trans_band)];
end

w = [zeros(1,fft_size/2-filter_size/2) window ...
     zeros(1,fft_size/2-filter_size/2)];
