function H = sfr_filters_f(S_f, c, pos_l, pos_c, pos_ref, r_m, ...
                           plane_wave, init_delay, pinv_condnums, norm_gains)
% sfr_filters_f - computes the SFR filters for a given loudspeaker array and
%                 set of control points at a given frequency
%
% Inputs
%  S_f:           temporal frequency [Hz]
%  c:             speed of sound [m/s]
%  pos_l:         array containing locations of loudspeakers
%                 (default 2D) Nx2
%  pos_c:         array of locations of control points (default 2D) Nx2
%                 used for MIMO inversion
%  pos_ref:       array of locations of reference points used for power
%                 averaging (can be same as pos_c)
%  r_m:           if plane_wave == 0,  r_m contains location of the primary
%                                      source (default 2D) 1x2
%                 if plane_wave =/= 0, r_m(1) contains azimuth angle of
%                                      plane wave propagation;
%                                      r_m(2) contains elevation
%  plane_wave:    plane wave primary source indicator
%                    == 0: spherical wave (point source)
%                   =/= 0: plane wave
%  init_delay:    initial delay for loudspeaker filters (to be accounted
%                 for)
%  pinv_condnums: condition numbers used for pseudoinverse and multiple
%                 thresholding
%  norm_gains:    control parameter for normalizing loudspeaker gains
%                   0: don't fix gains
%                   1: normalize for all control points (default)
%                   2: normalize for central control point
%
% Outputs
%  H:             vector with loudspeaker filters' complex values

% Author:         Mihailo Kolundzija (mihailo.kolundzija@epfl.ch)
% Date:           April 2011

N_l = size(pos_l,1);

% compute the propagation characteristic matrix (from all loudspeakers to
% all control points)

G = acoustic_transfer_matrix(S_f, c, pos_l, pos_c, 0);

% compute propagation characteristic from the desired source to all control
% points

A = acoustic_transfer_matrix(S_f, c, r_m, pos_c, plane_wave);
A = A .* exp(+1j*2*pi*S_f.*init_delay);

% determine thresholds from the value of the max singular value and the
% array of condition numbers (used for multiple thresholding)

max_sing_val = max(svd(G));
thresholds = max_sing_val ./ pinv_condnums;

% compute the loudspeaker filters (using possibly multiple thresholds)

H = zeros(N_l,1);
t_l = length(thresholds);
for i = 1:t_l
  H_tmp = pinv(G,thresholds(i)) * A;
  H = H + H_tmp ./ t_l;
end

% normalize gains to have a desired power on the reference line (1) or in
% the reference point (2)

if norm_gains == 1 % normalize on the reference line (multiple points)
  G = acoustic_transfer_matrix(S_f, c, pos_l, pos_ref, 0);
  A = acoustic_transfer_matrix(S_f, c, r_m, pos_ref, plane_wave);
  A_sfr = G * H; % responses of SFR in control points
  avg_gain = sum(abs(A).^2)/length(A);
  avg_sfr_gain = sum(abs(A_sfr).^2)/length(A_sfr);
  H = H .* sqrt(avg_gain / avg_sfr_gain);
elseif norm_gains == 2 % normalize in a single point
  % reference point's index in the array pos_c (mid point)
  ref_idx = round(size(pos_ref,1)/2);
  % transfer matrix to reference point
  G = acoustic_transfer_matrix(S_f, c, pos_l, pos_ref(ref_idx,:), 0);
  A_sfr = G * H; % responses of WFS in control points
  A = acoustic_transfer_matrix(S_f, c, r_m, pos_ref(ref_idx,:), ...
                               plane_wave);
  avg_gain = sum(abs(A).^2)/length(A);
  avg_sfr_gain = sum(abs(A_sfr).^2)/length(A_sfr);
  H = H .* sqrt(avg_gain / avg_sfr_gain);
end
