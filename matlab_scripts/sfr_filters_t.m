function [filters, delays, neg_delay] = sfr_filters_t(fft_size, ...
                                                      filter_size, ...
                                                      f_s, ...
                                                      c, ...
                                                      pos_l, ...
                                                      pos_c, ...
                                                      pos_ref, ...
                                                      r_m, ...
                                                      plane_wave, ...
                                                      pinv_condnums, ...
                                                      norm_gains, ...
                                                      smooth_len, ...
                                                      lp_filter, ...
                                                      prun_win)
% sfr_filters_t - computes the SFR filters for a given loudspeaker array and
%                 set of control points
%
% Inputs
%  fft_size:      size of the DFT used for filter computation
%  filter_size:   length of loudspeaker filters [samples]
%  f_s:           sampling frequency [Hz]
%  c:             speed of sound [m/s]
%  pos_l:         array of locations of loudspeakers (default 2D) Nx2
%  pos_c:         array of locations of control points (default 2D) Nx2
%                 used for MIMO inversion
%  pos_ref:       array of locations of reference points used for power
%                 averaging (can be same as pos_c)
%  r_m:           if plane_wave == 0,  r_m contains location of the primary
%                                      source (default 2D) 1x2
%                 if plane_wave =/= 0, r_m(1) contains azimuth angle of
%                                      plane wave propagation;
%                                      r_m(2) contains elevation
%  plane_wave:    plane wave primary source indicator
%                   == 0: spherical wave (point source)
%                  =/= 0: plane wave
%  pinv_condnums: condition numbers used for pseudoinverse and multiple
%                 thresholding
%  norm_gains:    control parameter for normalizing loudspeaker gains
%                  0: don't fix gains
%                  1: normalize for all control points (default)
%                  2: normalize for central control point
%  smooth_len:    length of the frequency-domain smoothing (MA) filter
%  lp_filter:     DFT of a low pass filter for cutting off freqs > 20 kHz
%  prun_win:      window for pruning the loudspeaker impulse responses
%
% Outputs
%  filters:        loudspeaker filters
%  delays:         delays associated to loudspeakers
%  neg_delay:      possible delay that needs to be subtracted to make
%                  delays non-negative

% Author:          Mihailo Kolundzija (mihailo.kolundzija@epfl.ch)
% Date:            April 2011

% number of loudspeakers
N_sfr = size(pos_l, 1);
% size of a single FFT bin
fft_bin_size = f_s/fft_size;
% primary source's frequencies for computing filters
S_F = 0:fft_bin_size:(fft_size/2)*fft_bin_size;
% initialize to zero, and change only if a negative delay is ignored
neg_delay = 0;

% determine initial (propagation) delays between the primary source and
% each loudspeaker; this extracts part of the delay of each loudspeaker
% filter and avoids phase wrap-around if the number of FFT bins is not high
% enough. Take the minimum of all initial delays as a common init_delay

if isempty(plane_wave) || plane_wave == 0
  plane_wave = 0;
  r_m_l = norm(r_m-pos_l(1,:)) * sign(pos_l(1,1)-r_m(1));
  for i = 2:size(pos_l,1)
    r = norm(r_m-pos_l(i,:)) * sign(pos_l(i,1)-r_m(1));
    if r < r_m_l
      r_m_l = r;
    end
  end
  init_delay_sfr = (r_m_l ./ c);
  init_delay_sfr = floor(init_delay_sfr .* f_s) ./ f_s;
else
  plane_wave = 1;
  init_delay_sfr = (pos_l(1,1) * cos(r_m(1)) * cos(r_m(2)) + ...
                    pos_l(1,2) * sin(r_m(1)) * cos(r_m(2))) / c;
  for i = 2:size(pos_l,1)
    del = (pos_l(i,1) * cos(r_m(1)) * cos(r_m(2)) + ...
           pos_l(i,2) * sin(r_m(1)) * cos(r_m(2))) / c;
    if del < init_delay_sfr
      init_delay_sfr = del;
    end
  end
  init_delay_sfr = floor(init_delay_sfr .* f_s) ./ f_s;
end

% for each frequency in S_F compute complex filters' response for all
% loudspeakers

i = 0;
H_sfr_f = zeros(size(pos_l,1),length(S_F));
for S_f = S_F
  i = i+1;
  [H_sfr_f(:,i)] = sfr_filters_f(...
                                 S_f, ...
                                 c, ...
                                 pos_l, ...
                                 pos_c, ...
                                 pos_ref, ...
                                 r_m, ...
                                 plane_wave, ...
                                 init_delay_sfr, ...
                                 pinv_condnums, ...
                                 norm_gains);
end
% H_sfr_f contains frequency-domain SFR filters

% smooth the frequency response filters by a moving average filter (in the
% frequency domain) having length smooth_len
for i = 1:N_sfr
  H_sfr_f(i,:) = zero_phase_mov_avg(H_sfr_f(i,:), smooth_len);
end

% compute SFR filter delays
H_sfr_phase = unwrap(angle(H_sfr_f).').';
delays = zeros(N_sfr,1);
delay_freqs = find(S_F > 0 & S_F < 20000);
for i = 1:N_sfr
  % compute delay of each SFR filter
  delays(i) = -H_sfr_phase(i,delay_freqs) / S_F(delay_freqs)/2/pi;
end

% compensate delays to have all filters' peaks at the same position
delays = floor(delays .* f_s) ./ f_s;
sfr_delay_compensation = delays-fft_size/(2*f_s);
for i = 1:N_sfr
  H_sfr_f(i,:) = H_sfr_f(i,:) .* exp(j*S_F*2*pi*sfr_delay_compensation(i));
end

% make sure delays are non-negative and no delays are used for plane waves
if init_delay_sfr < 0
  % there are negative delays (focused source) or a plane wave
  neg_delay = -init_delay_sfr; % negative delay which was dropped
  init_delay_sfr = 0;
end

% apply low-pass filter

% make sure it is a row vector
lp_filter = lp_filter(:).';
% assure it has same length as H_sfr_f
lp_filter = lp_filter(1:length(S_F));
H_sfr_f = H_sfr_f .* kron(lp_filter, ones(N_sfr,1));

% make the response of WFS filters real (make spectrum conjugate symmetric)
H_sfr_real = [H_sfr_f conj(fliplr(H_sfr_f(:,2:end-1)))];
H_sfr_t = real(ifft(H_sfr_real.').');

% prune (window) the obtained time-domain filters
window = prun_win(:).'; % make sure it is a row vector
H_sfr_t = H_sfr_t .* kron(window, ones(size(H_sfr_t,1),1));

% indexes of the filters' impulse responses to be preserved
filter_idx = (fft_size-filter_size)/2+1:(fft_size+filter_size)/2;
% extract the short filters
filters = H_sfr_t(:,filter_idx);

delays = delays + init_delay_sfr;
