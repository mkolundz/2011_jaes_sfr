%
%  This script performs a Monte-Carlo simulation in order to compare
%  the performance of WFS I, WFS II and SFR for three types of secondary
%  sources:
%    1. Focused sources (in front of the loudspeaker array)
%    2. Sources closely behind the loudspeaker array
%    3. Far-away sources modeled with plane waves.
%
%  Comparisons are done using aggregated magnitude and group delay
%  responses, with a clear indication of responses' variability at all
%  frequencies.

%  Author: Mihailo Kolundzija (mihailo.kolundzija@epfl.ch)
%  Date:   April 2011

clc
close all
clear all

% load global experiment parameters (stored in a file)
experiment_parameters;

%% compute the source positions and loudspeaker filters

% name of the file where parameters and filters are stored
filters_filename = 'wfs_sfr_filters.mat';

% test scenarios
% I) sources in front of the array
% II) sources closely behind the array
% III) plane waves
N_src = 10; % number of sources in each scenario
plane_wave_max_angle = 15/180*pi;

ldspk_select = ones(N_src*3, 1);
src_pos = zeros(N_src*3, 2);
src_plane_wave = zeros(N_src*3, 1);
% first N_src sources in zone I
x_max = x_l + ...
  (room_y/2-pos_l(1,2))/(contr_max_y-pos_l(1,2) * (r_c(1)-x_l));
src_pos(1:N_src,1) = x_l + (1:N_src)/N_src * (x_max - x_l);
src_pos(1:N_src,2) = pos_l(1,2) + ...
  (room_y/2-pos_l(1,2)) .* (src_pos(1:N_src,1)-x_l)./(x_max-x_l) + ...
  rand(N_src,1) .* (room_y/2-pos_l(1,2)) .* ...
  (x_max-src_pos(1:N_src,1)) ./ (x_max-x_l);
src_plane_wave(1:N_src) = 0;

% second N_src sources in zone II
src_area = [x_l-room_y x_l 0 room_y/2];
src_plane_wave(1+N_src:2*N_src) = 0;
src_pos(1+N_src:2*N_src,1) = src_area(1) + ...
  (0:N_src-1)/N_src * (src_area(2)-src_area(1));
src_pos(1+N_src:2*N_src,2) = src_area(3) + ...
  rand(N_src,1) * (src_area(4) - src_area(3));

% third N_src sources in zone III (plane waves)
src_plane_wave(2*N_src+1:3*N_src) = 1;
src_pos(2*N_src+1:3*N_src,1) = (0:N_src-1)/N_src*plane_wave_max_angle;

% compute the loudspeaker filters
compute_filters(src_pos, src_plane_wave, ldspk_select, filters_filename);


%% compute the responses

% name of the file where parameters and responses are stored
responses_filename = 'wfs_sfr_responses.mat';

dy =    .50; % distance between listening locations along y axis [m]
dx =      1; % distance between listening locations along x axis [m]
start_x = 6; % x coordinate of the first line where the response
             % is calculated [m]
compute_responses(dx, dy, start_x, filters_filename, responses_filename);


%% plot the magnitude and group delay frequency responses (percentile plots)

savefig = 0;
ticks = { 'b-', 'k--', 'k:', 'k-.', 'k--', 'k-.'};
linewidths = { 2.0, 1.5, 1.5, 1.5, 1, 1};

spectra_filename = 'spectra';
group_delay_filename = 'group_delay';

% length of the window for plot smoothing in the frequency domain
smooth_window =   3;

use_median      = 1; % 1: use median to show 'average' response
                     % 0: use mean to show average response

use_percentiles = 1; % 1: use percentiles to show variability
                     % 0: use variance/standard deviation to show
                     %    variability

% percentiles for plotting spread of magnitude and group delay responses
percentiles = [5 25 75 95];
num_percentiles = length(percentiles);
up_percentiles_color = [.75 .75 .75];
low_percentiles_color = [.5 .5 .5];

max_grp_del_err = 10; % maximum group delay error for plotting

% load responses from a file where they are stored
load(responses_filename, ...
     'wfs_a_grp_delay_err', 'wfs_s_grp_delay_err', ...
     'sfr_grp_delay_err', 'sfr_spectrum_norm', ...
     'wfs_a_spectrum_norm', 'wfs_s_spectrum_norm', ...
     'grp_del_freqs', ...
     'pos_c', 'N_x');

N_c = size(pos_c,1); % number of control points
N_src = size(src_pos, 1) / 3;

% convert `delay errors to miliseconds
wfs_a_grp_delay_err = wfs_a_grp_delay_err ./ f_s * 1000;
wfs_s_grp_delay_err = wfs_s_grp_delay_err ./ f_s * 1000;
sfr_grp_delay_err = sfr_grp_delay_err ./ f_s * 1000;

% plot responses for all three zones

t_start = tic;

for zone = 1:3

  % indexes of reference points used for computing response statistics
  % ref_pos_idxs = [(0*N_c/3+4):(0*N_c/3+6) (1*N_c/3+1):(2*N_c/3+N_c/3)];
  ref_start_x = 1;
  ref_end_x = N_x;
  ref_start_y = 1;
  ref_end_y = N_c/N_x;
  ref_pos_y = ref_start_y:ref_end_y;
  ref_pos_x = (ref_start_x:ref_end_x)-1;
  ref_pos_idxs = kron(ref_pos_x, (N_c/N_x).*ones(size(ref_pos_y))) + ...
                 kron(ones(size(ref_pos_x)), ref_pos_y);


  %ref_pos_idxs = (1*N_c/N_x+1):N_c;
  % indexes of sources used for computing response statistics
  src_idxs = 1:N_src;
  % indexes in the responses matrices (combining both sources and
  % reference points)
  res_idxs = (zone-1)*N_src*N_c + ...
             kron((src_idxs-1)*N_c, ones(size(ref_pos_idxs))) + ...
             kron(ones(size(src_idxs)), ref_pos_idxs);

  % compute group delay and magnitude spectrum means or medians
  if use_median == 1
    % group delay medians
    wfs_s_grp_delay_err_mean = median(wfs_s_grp_delay_err(res_idxs,:));
    wfs_a_grp_delay_err_mean = median(wfs_a_grp_delay_err(res_idxs,:));
    sfr_grp_delay_err_mean = median(sfr_grp_delay_err(res_idxs,:));
    % magnitude spectrum medians
    wfs_s_spectrum_norm_mean = median(wfs_s_spectrum_norm(res_idxs,:));
    wfs_a_spectrum_norm_mean = median(wfs_a_spectrum_norm(res_idxs,:));
    sfr_spectrum_norm_mean = median(sfr_spectrum_norm(res_idxs,:));
  else
    % group delay meand
    wfs_s_grp_delay_err_mean = mean(wfs_s_grp_delay_err(res_idxs,:));
    wfs_a_grp_delay_err_mean = mean(wfs_a_grp_delay_err(res_idxs,:));
    sfr_grp_delay_err_mean = mean(sfr_grp_delay_err(res_idxs,:));
    % magnitude spectrum means
    wfs_s_spectrum_norm_mean = mean(wfs_s_spectrum_norm(res_idxs,:));
    wfs_a_spectrum_norm_mean = mean(wfs_a_spectrum_norm(res_idxs,:));
    sfr_spectrum_norm_mean = mean(sfr_spectrum_norm(res_idxs,:));
  end

  if use_percentiles == 1
    % compute group delays 5% and 95% percentiles
    wfs_s_grp_delay_err_perc = prctile(wfs_s_grp_delay_err(res_idxs,:), ...
                                       percentiles);
    wfs_a_grp_delay_err_perc = prctile(wfs_a_grp_delay_err(res_idxs,:), ...
                                       percentiles);
    sfr_grp_delay_err_perc = prctile(sfr_grp_delay_err(res_idxs,:), ...
                                     percentiles);
    % compute spectrum 5% and 95% percentiles
    wfs_s_spectrum_norm_perc = prctile(wfs_s_spectrum_norm(res_idxs,:), ...
                                       percentiles);
    wfs_a_spectrum_norm_perc = prctile(wfs_a_spectrum_norm(res_idxs,:), ...
                                       percentiles);
    sfr_spectrum_norm_perc = prctile(sfr_spectrum_norm(res_idxs,:), ...
                                     percentiles);
  else
    % compute group delays std
    wfs_s_grp_delay_err_std = std(wfs_s_grp_delay_err(res_idxs,:));
    wfs_a_grp_delay_err_std = std(wfs_a_grp_delay_err(res_idxs,:));
    sfr_grp_delay_err_std = std(sfr_grp_delay_err(res_idxs,:));
    % compute group delay upper and lower margins
    wfs_s_grp_delay_err_perc = ...
      [wfs_s_grp_delay_err_mean-wfs_s_grp_delay_err_std; ...
      wfs_s_grp_delay_err_mean+wfs_s_grp_delay_err_std];
    wfs_a_grp_delay_err_perc = ...
      [wfs_a_grp_delay_err_mean-wfs_a_grp_delay_err_std; ...
      wfs_a_grp_delay_err_mean+wfs_a_grp_delay_err_std];
    sfr_grp_delay_err_perc = ...
      [sfr_grp_delay_err_mean-sfr_grp_delay_err_std; ...
      sfr_grp_delay_err_mean+sfr_grp_delay_err_std];

    % compute spectrum std
    wfs_s_spectrum_norm_std = std(wfs_s_spectrum_norm(res_idxs,:));
    wfs_a_spectrum_norm_std = std(wfs_a_spectrum_norm(res_idxs,:));
    sfr_spectrum_norm_std = std(sfr_spectrum_norm(res_idxs,:));
    % compute magnitude spectrum upper and lower margins
    wfs_s_spectrum_norm_perc = ...
      [wfs_s_spectrum_norm_mean-wfs_s_spectrum_norm_std; ...
       wfs_s_spectrum_norm_mean+wfs_s_spectrum_norm_std];
    wfs_a_spectrum_norm_perc = ...
      [wfs_a_spectrum_norm_mean-wfs_a_spectrum_norm_std; ...
       wfs_a_spectrum_norm_mean+wfs_a_spectrum_norm_std];
    sfr_spectrum_norm_perc = ...
      [sfr_spectrum_norm_mean-sfr_spectrum_norm_std; ...
       sfr_spectrum_norm_mean+sfr_spectrum_norm_std];
  end

  % smooth me(di)an and percentiles over frequencies
  for i = 1:num_percentiles
    wfs_s_grp_delay_err_perc(i,:) = ...
      zero_phase_mov_avg(wfs_s_grp_delay_err_perc(i,:), smooth_window);
    wfs_a_grp_delay_err_perc(i,:) = ...
      zero_phase_mov_avg(wfs_a_grp_delay_err_perc(i,:), smooth_window);
    sfr_grp_delay_err_perc(i,:) = ...
      zero_phase_mov_avg(sfr_grp_delay_err_perc(i,:), smooth_window);

    wfs_s_spectrum_norm_perc(i,:) = ...
      zero_phase_mov_avg(wfs_s_spectrum_norm_perc(i,:), smooth_window);
    wfs_a_spectrum_norm_perc(i,:) = ...
      zero_phase_mov_avg(wfs_a_spectrum_norm_perc(i,:), smooth_window);
    sfr_spectrum_norm_perc(i,:) = ...
      zero_phase_mov_avg(sfr_spectrum_norm_perc(i,:), smooth_window);
  end

  wfs_s_grp_delay_err_mean = zero_phase_mov_avg(wfs_s_grp_delay_err_mean, ...
                                                smooth_window);
  wfs_a_grp_delay_err_mean = zero_phase_mov_avg(wfs_a_grp_delay_err_mean, ...
                                                smooth_window);
  sfr_grp_delay_err_mean   = zero_phase_mov_avg(sfr_grp_delay_err_mean, ...
                                                smooth_window);

  wfs_s_spectrum_norm_mean = zero_phase_mov_avg(wfs_s_spectrum_norm_mean, ...
                                                smooth_window);
  wfs_a_spectrum_norm_mean = zero_phase_mov_avg(wfs_a_spectrum_norm_mean, ...
                                                smooth_window);
  sfr_spectrum_norm_mean   = zero_phase_mov_avg(sfr_spectrum_norm_mean, ...
                                                smooth_window);

  % compute max and min magnitude specta values in order to use same axes
  % for all (sub)plots
  max_upper_spectrum = ...
    max(max(max(wfs_a_spectrum_norm_perc(num_percentiles,:)), ...
    max(wfs_s_spectrum_norm_perc(num_percentiles,:))), ...
    max(sfr_spectrum_norm_perc(num_percentiles,:)));
  min_lower_spectrum = ...
    min(min(min(wfs_a_spectrum_norm_perc(1,:)), ...
    min(wfs_s_spectrum_norm_perc(1,:))), ...
    min(sfr_spectrum_norm_perc(1,:)));

  plot_freqs = (grp_del_freqs);

  figure((zone-1)*2+1);
  % plot normalized magnitude spectrum of WFS with all loudspeakers
  subplot(3,1,1);
  fill([plot_freqs fliplr(plot_freqs)], ...
       [20*log10(wfs_a_spectrum_norm_perc(1,:)) ...
        20*log10(fliplr(wfs_a_spectrum_norm_perc(num_percentiles,:)))], ...
       low_percentiles_color);
  hold on
  if num_percentiles > 2
    fill([plot_freqs fliplr(plot_freqs)], ...
         [20*log10(wfs_a_spectrum_norm_perc(2,:)) ...
          20*log10(fliplr(wfs_a_spectrum_norm_perc(3,:)))], ...
         up_percentiles_color);
  end
  plot(plot_freqs, 20*log10(wfs_a_spectrum_norm_mean), ...
  ticks{1}, 'linewidth', linewidths{1});
  hold off
  axis([plot_freqs(1) plot_freqs(end) ...
        20*log10(min_lower_spectrum) 20*log10(max_upper_spectrum)]);
  grid on
  set(gca, 'Xscale', 'log');
  xlabel('f [Hz]');
  ylabel('$Y/Y_d$ [dB]', 'Interpreter', 'latex');
  title('WFS I');

  subplot(3,1,2);
  % plot normalized magnitude spectrum of WFS with a subset of loudspeakers
  fill([plot_freqs fliplr(plot_freqs)], ...
       [20*log10(wfs_s_spectrum_norm_perc(1,:)) ...
        20*log10(fliplr(wfs_s_spectrum_norm_perc(num_percentiles,:)))], ...
       low_percentiles_color);
  hold on
  if num_percentiles > 2
    fill([plot_freqs fliplr(plot_freqs)], ...
         [20*log10(wfs_s_spectrum_norm_perc(2,:)) ...
          20*log10(fliplr(wfs_s_spectrum_norm_perc(3,:)))], ...
         up_percentiles_color);
  end
  plot(plot_freqs, 20*log10(wfs_s_spectrum_norm_mean), ...
       ticks{1}, 'linewidth', linewidths{1});
  hold off
  axis([plot_freqs(1) plot_freqs(end) ...
        20*log10(min_lower_spectrum) 20*log10(max_upper_spectrum)]);
  grid on
  set(gca, 'Xscale', 'log');
  xlabel('f [Hz]');
  ylabel('$Y/Y_d$ [dB]', 'Interpreter', 'latex');
  title('WFS II');

  subplot(3,1,3);
  % plot normalized magnitude spectrum of SFR
  fill([plot_freqs fliplr(plot_freqs)], ...
       [20*log10(sfr_spectrum_norm_perc(1,:)) ...
        20*log10(fliplr(sfr_spectrum_norm_perc(num_percentiles,:)))], ...
       low_percentiles_color);
  hold on
  if num_percentiles > 2
    fill([plot_freqs fliplr(plot_freqs)], ...
        [20*log10(sfr_spectrum_norm_perc(2,:)) ...
         20*log10(fliplr(sfr_spectrum_norm_perc(3,:)))], ...
         up_percentiles_color);
  end
  plot(plot_freqs, 20*log10(sfr_spectrum_norm_mean), ...
       ticks{1}, 'linewidth', linewidths{1});
  hold off
  axis([plot_freqs(1) plot_freqs(end) ...
        20*log10(min_lower_spectrum) 20*log10(max_upper_spectrum)]);
  grid on
  set(gca, 'Xscale', 'log');
  xlabel('f [Hz]');
  ylabel('$Y/Y_d$ [dB]', 'Interpreter', 'latex');
  title('SFR');

  if savefig == 1
    print('-depsc', '-r200', [spectra_filename, '_', num2str(zone), '.eps']);
  end

  % compute maximum and minimum group delays (and clip to upper and lower
  % values for nicer plots) to define same axes for all (sub)plots
  max_upper_del_err = ...
    max(max(max(wfs_a_grp_delay_err_perc(num_percentiles,:)), ...
    max(wfs_s_grp_delay_err_perc(num_percentiles,:))), ...
    max(sfr_grp_delay_err_perc(num_percentiles,:)));
  min_lower_del_err = ...
    min(min(min(wfs_a_grp_delay_err_perc(1,:)), ...
    min(wfs_s_grp_delay_err_perc(1,:))), ...
    min(sfr_grp_delay_err_perc(1,:)));
  max_upper_del_err = sign(max_upper_del_err) * ...
    min(max_grp_del_err, abs(max_upper_del_err));
  min_lower_del_err = sign(min_lower_del_err) * ...
    min(max_grp_del_err, abs(min_lower_del_err));

  figure((zone-1)*2+2);
  subplot(3,1,1);
  % plot group delay error of WFS with all loudspeakers
  fill([plot_freqs fliplr(plot_freqs)], ...
       [(wfs_a_grp_delay_err_perc(1,:)) ...
       (fliplr(wfs_a_grp_delay_err_perc(num_percentiles,:)))], ...
       low_percentiles_color);
  hold on
  if num_percentiles > 2
    fill([plot_freqs fliplr(plot_freqs)], ...
         [(wfs_a_grp_delay_err_perc(2,:)) ...
         (fliplr(wfs_a_grp_delay_err_perc(3,:)))], ...
         up_percentiles_color);
  end
  plot(plot_freqs, (wfs_a_grp_delay_err_mean), ...
       ticks{1}, 'linewidth', linewidths{1});
  hold off
  axis([plot_freqs(1) plot_freqs(end) ...
       (min_lower_del_err) (max_upper_del_err)]);
  grid on
  set(gca, 'Xscale', 'log');
  xlabel('f [Hz]');
  ylabel('$\tau_g - \tau_g^d$ [ms]', 'Interpreter', 'latex');
  title('WFS I');

  subplot(3,1,2);
  % plot group delay error of WFS with a subset of loudspeakers
  fill([plot_freqs fliplr(plot_freqs)], ...
       [(wfs_s_grp_delay_err_perc(1,:)) ...
        (fliplr(wfs_s_grp_delay_err_perc(num_percentiles,:)))], ...
       low_percentiles_color);
  hold on
  if num_percentiles > 2
    fill([plot_freqs fliplr(plot_freqs)], ...
         [(wfs_s_grp_delay_err_perc(2,:)) ...
          (fliplr(wfs_s_grp_delay_err_perc(3,:)))], ...
         up_percentiles_color);
  end
  plot(plot_freqs, (wfs_s_grp_delay_err_mean), ...
       ticks{1}, 'linewidth', linewidths{1});
  hold off
  axis([plot_freqs(1) plot_freqs(end) ...
       (min_lower_del_err) (max_upper_del_err)]);
  grid on
  set(gca, 'Xscale', 'log');
  xlabel('f [Hz]');
  ylabel('$\tau_g - \tau_g^d$ [ms]', 'Interpreter', 'latex');
  title('WFS II');

  subplot(3,1,3);
  % plot group delay error of SFR
  fill([plot_freqs fliplr(plot_freqs)], ...
       [(sfr_grp_delay_err_perc(1,:)) ...
        (fliplr(sfr_grp_delay_err_perc(num_percentiles,:)))], ...
       low_percentiles_color);
  hold on
  if num_percentiles > 2
    fill([plot_freqs fliplr(plot_freqs)], ...
         [(sfr_grp_delay_err_perc(2,:)) ...
          (fliplr(sfr_grp_delay_err_perc(3,:)))], ...
         up_percentiles_color);
  end
  plot(plot_freqs, (sfr_grp_delay_err_mean), ...
       ticks{1}, 'linewidth', linewidths{1});
  hold off
  axis([plot_freqs(1) plot_freqs(end) ...
       (min_lower_del_err) (max_upper_del_err)]);
  grid on
  set(gca, 'Xscale', 'log');
  xlabel('f [Hz]');
  ylabel('$\tau_g - \tau_g^d$ [ms]', 'Interpreter', 'latex');
  title('SFR');

  if savefig == 1
    print('-depsc', '-r200', ...
          [group_delay_filename, '_', num2str(zone), '.eps']);
  end
end

t_end = toc(t_start);

