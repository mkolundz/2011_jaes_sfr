%
%  This script compares WFS I, WFS II and SFR using
%  sound field snapshots in three scenarios:
%  1. Harmonic Type 2 source (behind the array) with frequency 500 Hz
%  2. Harmonic Type 2 source (behind the array) with frequency 2 kHz
%  3. Type 3 source (plane wave) emitting a train of low-pass pulses.

%  Author: Mihailo Kolundzija (mihailo.kolundzija@epfl.ch)
%  Date:   April 2011

clc
close all

% load global experiment parameters
experiment_parameters;


%% compute the source positions and loudspeaker filters

% name of the file where parameters and filters are stored
filters_filename = 'wfs_sfr_filters_snapshot.mat';

% set the source positions, plane wave and loudspeaker select indicators
src_pos = [3 1; 5 2; 0 0];
src_plane_wave = [0; 0; 1];
ldspk_select = [1; 1; 1];

% compute the loudspeaker filters
compute_filters(src_pos, src_plane_wave, ldspk_select, filters_filename);


%% plot the snapshots for all

savefig = 0;
bw_figs = 1; % 0: color plots; otherwise: black and white plots

output_folder =    'outputs/';

% index of the tested source
src_idxs = [1 1 3];

% frequency of the source [Hz]
S_f = [500 2000 0];

% 1: train of impulses
% 2: pink noise
% 3: sinusoid
% 4: train of filtered pulses
% 5: single pulse
stimulus_types = [3 3 4];

stimulus_strings = {'sinusoid', 'sinusoid', 'pulsetrain'};

% period of the pulse train in seconds
pulse_train_period = .004;
% length in seconds
stimulus_length = 2;

% maximum passband frequency for low-pass pulses
f_m = 3000;
% minimum stopband frequency for low-pass pulses
f_m_r = f_m + 1000;
% boundaries of the area where the field's snapshot is plotted
plot_area = [3 room_x-1 0 room_y];
% the moment to take the snapshot [s]
t_snapshot = 2 * room_x / c;
% x coordinate of listening points
x_listen = [8 8 8]';
% y coordinates of listening points
y_listen = [0 room_y/2 room_y]';
% listening points
pos_c = [x_listen y_listen];
% spatial sampling interval in y [m] (used for plotting shapshots)
delta  =            0.02;

% normalization type for response waveforms
%  1: energy normalization relative to input
%  2: maximum amplitude normalization relative to input (avoids clipping)
norm_type = 2;

% load source parameters, filters, delays etc.
load(filters_filename, 'sfr_filters_array', 'sfr_delays_array');
load(filters_filename, 'wfs_filters_s_array', 'wfs_delays_s_array');
load(filters_filename, 'wfs_filters_a_array', 'wfs_delays_a_array');
load(filters_filename, 'max_neg_delays_array');
load(filters_filename, 'src_pos', 'src_plane_wave');
load(filters_filename, 'wfs_speakers_s_array');
load(filters_filename, 'wfs_speakers_a_array');
load(filters_filename, 'sfr_speakers_array');

for cnt = 1:length(src_idxs)
  src_idx = src_idxs(cnt);
  s_f = S_f(cnt);
  stimulus_type = stimulus_types(cnt);

  % form the stimulus signal
  stimulus_params(2:3) = [f_m f_m_r];
  if stimulus_type == 3
    stimulus_params(1) = s_f;
  else
    stimulus_params(1) = pulse_train_period;
  end
  s = make_stimulus(f_s, stimulus_length, stimulus_type, stimulus_params);

  % load the filters for the desired source
  r_m = src_pos(src_idx, :);
  plane_wave = src_plane_wave(src_idx);
  wfs_filters_a = squeeze(wfs_filters_a_array(src_idx,:,:));
  wfs_filters_s = squeeze(wfs_filters_s_array(src_idx,:,:));
  sfr_filters = squeeze(sfr_filters_array(src_idx,:,:));
  wfs_delays_a = squeeze(wfs_delays_a_array(src_idx,:));
  wfs_delays_s = squeeze(wfs_delays_s_array(src_idx,:));
  sfr_delays = squeeze(sfr_delays_array(src_idx,:));
  max_neg_delay = max_neg_delays_array(src_idx);
  wfs_speakers_s = wfs_speakers_s_array(src_idx,:);
  wfs_speakers_a = wfs_speakers_a_array(src_idx,:);
  sfr_speakers = sfr_speakers_array(src_idx,:);

  % plot wfs and sfr sound fields

  % compute SFR, WFS, and desired snapshot
  sfr_snapshot = soundfield_snapshot(f_s, c, pos_l, 0, plot_area, ...
                                     delta, t_snapshot, sfr_filters, ...
                                     sfr_delays, s, 1/delta/40);
  wfs_a_snapshot = soundfield_snapshot(f_s, c, pos_l, 0, plot_area, ...
                                       delta, t_snapshot, wfs_filters_a, ...
                                       wfs_delays_a, s, 1/delta/40);
  wfs_s_snapshot = soundfield_snapshot(f_s, c, pos_l, 0, plot_area, ...
                                       delta, t_snapshot, wfs_filters_s, ...
                                       wfs_delays_a, s, 1/delta/40);
  desired_snapshot = soundfield_snapshot(f_s, c, r_m, plane_wave, ...
                                         plot_area, delta, t_snapshot, 1, ...
                                         filter_size/2/f_s, s, 1/delta/40);

  % plot sound fields' snapshots

  max_snapshot = max(max(abs(sfr_snapshot(:))), ...
                     max(abs(wfs_a_snapshot(:))));
  max_snapshot = max(max(abs(wfs_s_snapshot(:))), max_snapshot);
  max_snapshot = max(max(abs(desired_snapshot(:))), max_snapshot);

  sc = [-max_snapshot max_snapshot];
  x_area = plot_area(1):delta:plot_area(2);
  y_area = plot_area(3):delta:plot_area(4);

  cmp = flipud(gray);

  figure(cnt);
  if bw_figs
    colormap(cmp);
  else
    colormap(jet);
  end

  % if bw_figs % print figures for the paper
  % desired snapshot
  subplot(2,2,1);
  plot_soundfield_snapshot(x_area, y_area, desired_snapshot, sc, ...
                           [], r_m, [], plane_wave, '(a)');
  % WFS with all loudspeakers snapshot
  subplot(2,2,2);
  plot_soundfield_snapshot(x_area, y_area, wfs_a_snapshot, sc, ...
                           pos_l(find(wfs_speakers_a>0),:), r_m, [], 1, '(b)');
  % WFS with a subset of loudspeakers snapshot
  subplot(2,2,3);
  plot_soundfield_snapshot(x_area, y_area, wfs_s_snapshot, sc, ...
                           pos_l(find(wfs_speakers_s>0),:), r_m, [], 1, '(c)');
  % SFR snapshot
  subplot(2,2,4);
  plot_soundfield_snapshot(x_area, y_area, sfr_snapshot, sc, ...
                           pos_l(find(sfr_speakers>0),:), r_m, [], 1, '(d)');

  if stimulus_type == 3
    stimulus_string_suffix = strcat('_', num2str(s_f), 'Hz');
  else
    stimulus_string_suffix = '';
  end

  if savefig % && bw_figs
    print('-depsc', '-r300', ...
          ['snapshot_', stimulus_strings{cnt}, ...
          stimulus_string_suffix, '.eps']);
  end

end
