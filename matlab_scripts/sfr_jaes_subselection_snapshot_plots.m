%
%  This script compares snapshots of sound field reproduced using SFR
%  with and without loudspeaker subset selection. The reproduced source
%  is of Type 2 (behind the array), and the snapshot is given when
%  reproducing a harmonic source at frequency 500 Hz.
%
%  It also compares the responses of WFS and SFR in three points on a
%  listening line (mid point and the two ends) when reproducing a
%  Type 2 (behind the array) point source. Ideally, the responses
%  would be perfect pulses in any listening point, i.e. they would
%  have flat frequency characteristics.

%  Author: Mihailo Kolundzija (mihailo.kolundzija@epfl.ch)
%  Date:   April 2011

clc
close all
clear all

% load parameters for all experiments
experiment_parameters;

% name of the file where parameters and filters are stored
filters_filename = 'wfs_sfr_filters_subselection_compare.mat';

% set the source positions, plane wave and loudspeaker select indicators
src_pos = [3 1; 3 1; 0 0];
src_plane_wave = [0; 0; 1];
ldspk_select = [1; 0; 1];

% compute the loudspeaker filters
compute_filters(src_pos, src_plane_wave, ldspk_select, filters_filename);


%% compute snapshots w/ & w/o loudspeaker selection and responses in a ref point

savefig = 0;
bw = 1; % 0: color plots; otherwise: black and white plots

% index of the tested source
src_idxs = [1 2];

% frequency of the source [Hz]
s_f = 500;

% 1: train of impulses
% 2: pink noise
% 3: sinusoid
% 4: train of filtered pulses
% 5: single pulse
stimulus_type = 3;

% period of the pulse train in seconds
pulse_train_period = .004;
% length in seconds
stimulus_length = 2;

% maximum passband frequency for low-pass pulses
f_m = 3000;
% minimum stopband frequency for low-pass pulses
f_m_r = f_m + 1000;
% boundaries of the area where the field's snapshot is plotted
plot_area = [3 room_x-1 0 room_y];
% the moment to take the snapshot [s]
t_snapshot = 2 * room_x / c;
% x coordinate of listening line
x_listen = 8;
% y coordinate of three contol points
y_listen = [0; 2; 4];
% distance between control points on the listening line [m]
delta_c = 0.2;
% spatial sampling interval in y [m] (used for plotting shapshots)
delta  =  0.02;

% frequencies for spectrum calculation
spectrum_freqs = 25:25:20000;

% normalization type for response waveforms
%  1: energy normalization relative to input
%  2: maximum amplitude normalization relative to input (avoids clipping)
norm_type = 2;

stimulus_params(2:3) = [f_m f_m_r];
stimulus_params(1) = s_f;
s = make_stimulus(f_s, stimulus_length, stimulus_type, stimulus_params);

% load source parameters, filters, delays etc.
load(filters_filename, 'sfr_filters_array', 'sfr_delays_array');
load(filters_filename, 'wfs_filters_s_array', 'wfs_delays_s_array');
load(filters_filename, 'wfs_filters_a_array', 'wfs_delays_a_array');
load(filters_filename, 'max_neg_delays_array');
load(filters_filename, 'src_pos', 'src_plane_wave');
load(filters_filename, 'wfs_speakers_s_array');
load(filters_filename, 'wfs_speakers_a_array');
load(filters_filename, 'sfr_speakers_array');

% load the filters for the desired source
r_m = src_pos(1, :);
plane_wave = 0;

% compute the desired snapshot
desired_snapshot = soundfield_snapshot(f_s, c, r_m, plane_wave, ...
                                       plot_area, delta, t_snapshot, 1, ...
                                       filter_size/2/f_s, s, 1/delta/40);

% compute the snapshot of SFR with subset selection
max_neg_delay_subset = max_neg_delays_array(1);
sfr_filters_subset = squeeze(sfr_filters_array(1,:,:));
sfr_delays_subset = squeeze(sfr_delays_array(1,:));
sfr_speakers_subset = sfr_speakers_array(1,:);
sfr_snapshot_subset = soundfield_snapshot(f_s, c, pos_l, 0, plot_area, ...
                                          delta, t_snapshot, ...
                                          sfr_filters_subset, ...
                                          sfr_delays_subset, s, 1/delta/40);

% compute the snapshot of SFR without subset selection
max_neg_delay_full = max_neg_delays_array(2);
sfr_filters_full = squeeze(sfr_filters_array(2,:,:));
sfr_delays_full = squeeze(sfr_delays_array(2,:));
sfr_speakers_full = sfr_speakers_array(2,:);
sfr_snapshot_full = soundfield_snapshot(f_s, c, pos_l, 0, plot_area, ...
                                        delta, t_snapshot, ...
                                        sfr_filters_full, ...
                                        sfr_delays_full, s, 1/delta/40);

max_snapshot = max(max(abs(sfr_snapshot_subset(:))), ...
  max(abs(sfr_snapshot_full(:))));
max_snapshot = max(max(abs(desired_snapshot(:))), max_snapshot);

sc = [-max_snapshot max_snapshot];
x_area = plot_area(1):delta:plot_area(2);
y_area = plot_area(3):delta:plot_area(4);

% compute frequency responses on the listening line
clear pos_c

% dense set of control points on the listening line
pos_c = (plot_area(3):delta_c:plot_area(4))';
pos_c = [x_listen*ones(length(pos_c),1) pos_c];

% make stimulus that is a single pulse
stimulus = make_stimulus(f_s, 0.2, 5, []);

% compute responses on the listening line
[desired_response] = ...
  snd_field_responses(f_s, c, r_m, pos_c, plane_wave, stimulus, [1 0], ...
                      max_neg_delay_full+filter_size/2/f_s, 0);
[sfr_full_response] = ...
  snd_field_responses(f_s, c, pos_l(find(sfr_speakers_full>0),:), pos_c, ...
                      plane_wave, stimulus, sfr_filters_full, ...
                      sfr_delays_full, 0);
[sfr_subset_response] = ...
  snd_field_responses(f_s, c, pos_l(find(sfr_speakers_subset>0),:), pos_c, ...
                      plane_wave, stimulus, sfr_filters_subset, ...
                      sfr_delays_subset, 0);

% compute the responses for the considered frequency s_f
for j = 1:size(pos_c,1)
  tmp = abs(freqz(desired_response(j,:), 1, [s_f s_f+10], f_s));
  desired_spectrum(j) = tmp(1);
  tmp = abs(freqz(sfr_full_response(j,:), 1, [s_f s_f+10], f_s));
  sfr_full_spectrum(j) = tmp(1);
  tmp = abs(freqz(sfr_subset_response(j,:), 1, [s_f s_f+10], f_s));
  sfr_subset_spectrum(j) = tmp(1);
end

%% compute magnitude frequency responses in three control points
% control points' location
ctr_pts = [x_listen*ones(size(y_listen)) y_listen];
% location of the primary source
r_m = src_pos(3,:);
plane_wave = src_plane_wave(3);
max_neg_delay = max_neg_delays_array(3);

% compute the desired response
[desired_response_ctr] = ...
  snd_field_responses(f_s, c, r_m, ctr_pts, plane_wave, stimulus, [1 0], ...
                      max_neg_delay+filter_size/2/f_s, 0);

% compute the snapshot of SFR with subset selection
wfs_filters = squeeze(wfs_filters_s_array(3,:,:));
wfs_delays = squeeze(wfs_delays_s_array(3,:));
wfs_speakers = wfs_speakers_s_array(3,:);
[wfs_response_ctr] = snd_field_responses(f_s, c, ...
                                         pos_l(find(wfs_speakers>0),:), ...
                                         ctr_pts, 0, stimulus, wfs_filters, ...
                                         wfs_delays, 0);

% compute the snapshot of SFR without subset selection
max_neg_delay_sfr = max_neg_delays_array(3);
sfr_filters = squeeze(sfr_filters_array(3,:,:));
sfr_delays = squeeze(sfr_delays_array(3,:));
sfr_speakers = sfr_speakers_array(3,:);
[sfr_response_ctr] = snd_field_responses(f_s, c, ...
                                         pos_l(find(sfr_speakers>0),:), ...
                                         ctr_pts, 0, stimulus, sfr_filters, ...
                                         sfr_delays, 0);

% compute the frequency responses in the three control points
for j = 1:size(ctr_pts,1)
  desired_spectrum_ctr(j,:) = ...
    abs(freqz(desired_response_ctr(j,:), 1, spectrum_freqs, f_s));
  wfs_spectrum_ctr(j,:) = ...
    abs(freqz(wfs_response_ctr(j,:), 1, spectrum_freqs, f_s));
  sfr_spectrum_ctr(j,:) = ...
    abs(freqz(sfr_response_ctr(j,:), 1, spectrum_freqs, f_s));
end


%% plot snapshots with amplitude on the reference line comparison

cmp = flipud(gray);

figure(1);
if bw
  colormap(cmp);
else
  colormap(jet);
end

% desired snapshot
subplot(2,2,1);
plot_soundfield_snapshot(x_area, y_area, desired_snapshot, sc, ...
                         [], r_m, [], plane_wave, '(a)');
hold on
line([pos_c(1,1) pos_c(end,1)], [pos_c(1,2) pos_c(end,2)], ...
     'Color',[0 0 0]);
hold off
% SFR with all loudspeakers snapshot
subplot(2,2,2);
plot_soundfield_snapshot(x_area, y_area, sfr_snapshot_full, sc, ...
                         pos_l(find(sfr_speakers_full>0),:), r_m, [], ...
                         plane_wave, '(b)');
hold on
line([pos_c(1,1) pos_c(end,1)], [pos_c(1,2) pos_c(end,2)], ...
     'Color',[0 0 0]);
hold off
% SFR with a subset of loudspeakers snapshot
subplot(2,2,3);
plot_soundfield_snapshot(x_area, y_area, sfr_snapshot_subset, sc, ...
                         pos_l(find(sfr_speakers_subset>0),:), r_m, [], ...
                         plane_wave, '(c)');
hold on
line([pos_c(1,1) pos_c(end,1)], [pos_c(1,2) pos_c(end,2)], ...
     'Color',[0 0 0]);
hold off
% comparing magnitude responses on the reference line at frequency s_f
subplot(2,2,4);
plot(pos_c(:,2), 20*log10(desired_spectrum), 'b-', 'linewidth', 1);
hold on
plot(pos_c(:,2), 20*log10(sfr_full_spectrum), 'k-', 'linewidth', 1.5);
plot(pos_c(:,2), 20*log10(sfr_subset_spectrum), 'r--', 'linewidth', 1.5);
hold off
title('(d)');
grid on
legend('desired', 'SFR full', 'SFR subset', 'location', 'southwest');

if savefig
  print('-depsc', ['subselection_compare.eps']);
end

%% compare the frequency responses in the reference points
figure(2)
subplot(3,1,1);
semilogx(spectrum_freqs, ...
         20*log10(wfs_spectrum_ctr(1,:) ./ desired_spectrum_ctr(1,:)), ...
         'k--', 'linewidth', 1.5);
hold on
semilogx(spectrum_freqs, ...
         20*log10(sfr_spectrum_ctr(1,:) ./ desired_spectrum_ctr(1,:)), ...
         'b-', 'linewidth', 1.5);
hold off
grid on
axis([100 20000 -30 10]);
xlabel('f [Hz]');
ylabel('$Y/Y_d$ [dB]', 'Interpreter', 'latex');
title('(a)');

subplot(3,1,2);
semilogx(spectrum_freqs, ...
         20*log10(wfs_spectrum_ctr(2,:) ./ desired_spectrum_ctr(2,:)), ...
         'k--', 'linewidth', 1.5);
hold on
semilogx(spectrum_freqs, ...
         20*log10(sfr_spectrum_ctr(2,:) ./ desired_spectrum_ctr(2,:)), ...
         'b-', 'linewidth', 1.5);
hold off
grid on
axis([100 20000 -30 10]);
xlabel('f [Hz]');
ylabel('$Y/Y_d$ [dB]', 'Interpreter', 'latex');
title('(b)');
legend('WFS', 'SFR', 'location', 'southwest');

subplot(3,1,3);
semilogx(spectrum_freqs, ...
         20*log10(wfs_spectrum_ctr(3,:) ./ desired_spectrum_ctr(3,:)), ...
         'k--', 'linewidth', 1.5);
hold on
semilogx(spectrum_freqs, ...
         20*log10(sfr_spectrum_ctr(3,:) ./ desired_spectrum_ctr(3,:)), ...
         'b-', 'linewidth', 1.5);
hold off
grid on
axis([100 20000 -30 10]);
xlabel('f [Hz]');
ylabel('$Y/Y_d$ [dB]', 'Interpreter', 'latex');
title('(c)');

if savefig
  print('-depsc', ['spectra.eps']);
end
