function field_resps = snd_field_responses(f_s, ...
                                           c, ...
                                           pos_l, ...
                                           pos_c, ...
                                           plane_wave, ...
                                           stimulus, ...
                                           l_filters, ...
                                           l_delays, ...
                                           norm_type)
% snd_field_responses - computes responses to a stimulus of a desired source,
%                       WFS and SFR setups in a set of control points
%
% Inputs
%  f_s:           sampling frequency [Hz]
%  c:             speed of sound [m/s]
%  pos_l:         if plane_wave == 1,  pos_l(1) contains azimuth angle of
%                                      plane wave propagation,
%                                      pos_l(2) contains elevation angle
%                 if plane_wave =/= 1, pos_l is an array containing
%                                      locations of
%                                      loudspeakers (default 2D) Nx2
%  pos_c:         array containing locations of control points
%                 (default 2D) Nx2
%  plane_wave:    non-zero value indicates that a propagating wave
%                 is a plane wave
%
%  stimulus:       excitation waveform
%  l_filters:     a set of time-domain secondary source (loudspeaker)
%                 filters
%  l_delays:      a set of initial delays of secondary sources
%                 (loudspeakers)
%  norm_type:     normalization selection for final responses
%                   1: normalize output to have the same energy
%                      as the input
%                   2: normalize to have the same maximum amplitude (avoids
%                      clipping)
% Outputs
%  field_resps:   responses in locations given by pos_c

% Author:         Mihailo Kolundzija (mihailo.kolundzija@epfl.ch)
% Date:           April 2011


N_c = size(pos_c, 1);
N_l = size(pos_l, 1);

if isempty(plane_wave) || plane_wave == 0
    plane_wave = 0;
else
    plane_wave = 1;
end

max_filt_delay = max(l_delays); % maximum delay of all filters

% compute matrix with distances between secondary sources (loudspeakers)
% and listening points
if plane_wave == 0
    r_l = zeros(N_c,N_l);
    for i = 1:size(pos_l,2)
        M = ones(N_c,1) * pos_l(:,i)';
        N = pos_c(:,i) * ones(1,N_l);
        r_l = r_l + (M-N).^2;
    end
    r_l = sqrt(r_l);
    prop_delay = r_l ./ c;
else
    prop_delay = (pos_c(:,1) .* cos(pos_l(1)) .* cos(pos_l(2)) + ...
                  pos_c(:,2) .* sin(pos_l(1)) .* cos(pos_l(2)))/c;
    if min(prop_delay) < 0
        prop_delay = prop_delay + min(prop_delay);
    end
end

% max delays (including propagation and filtering delays)
max_delay = max(prop_delay(:)) + max_filt_delay;
% accomodate for the maximum delay, but shift left by min_delay samples
% (leading zeros not important)
common_delay = max_delay;
common_delay_int = ceil(common_delay .* f_s);

field_resps = zeros(N_c, ...
  common_delay_int+size(stimulus,2)+size(l_filters,2));
% compute responses in all listening points
for i = 1:N_c
    % stimulus will include the leading (delay) zeros, the stimulus signal,
    % and trailing zeros so that the convolution is not truncated
    stimulus_delayed = ...
      zeros(size(l_filters,1), ...
            common_delay_int+size(stimulus,2)+size(l_filters,2));
    for ii = 1:N_l
        % propagation and filtering delay
        filt_and_prop_delay = floor((prop_delay(i,ii) + ...
                                    l_delays(ii)).*f_s);
        stimulus_delayed(ii,1:filt_and_prop_delay+size(stimulus,2)) = ...
          [zeros(1,filt_and_prop_delay) stimulus(1,:)];
        % compute the attenuation due to propagation (1/(4r\pi)
        if plane_wave == 0
            prop_attenuation = 1 ./ (4 * pi * r_l(i,ii));
        else
            prop_attenuation = 1;
        end
        field_resps(i,:) = prop_attenuation .* ...
          fftfilt(l_filters(ii,:),stimulus_delayed(ii,:)) + ...
          field_resps(i,:);
    end

end

if norm_type == 1
    % normalize outputs so that the maximum of all has equal energy as the
    % input (make sense for listening)
    max_en = max(sum(field_resps.^2,2));
    input_en = sum(stimulus.^2);
    if max_en > input_en
        field_resps = field_resps .* sqrt(input_en / max_en);
    end
elseif norm_type == 2
    % normalize outputs so that the maximum of all has the same maximum
    % amplitude as the input (avoid clipping)
    max_en = max(abs(field_resps(:)));
    input_en = max(abs(stimulus(:)));
    if max_en > input_en
        field_resps = field_resps .* (input_en / max_en);
    end
end


