function field_snapshot = soundfield_snapshot(f_s, ...
                                              c, ...
                                              pos_l, ...
                                              plane_wave, ...
                                              plot_area, ...
                                              delta, ...
                                              t, ...
                                              ldspk_filters, ...
                                              ldspk_delays, ...
                                              stim, ...
                                              max_amp)
% soundfield_snapshot - computes the sound pressure field at time instant t
%                       of a loudspeaker array located in pos_l, using filters
%                       H, emitting waveform stimulus. Snapshot is computed in
%                       the area defined by plot_area, on a grid of
%                       points spaced at delta in both x and y directions
%
% Inputs
%  f_s:            sampling frequency [Hz]
%  c:              speed of sound [m/s]
%  pos_l:          if plane_wave == 0,  pos_l contains location of the
%                                       primary source (default 2D) 1x2
%                  if plane_wave =/= 0, pos_l(1) contains azimuth angle of
%                                       plane wave propagation;
%                                       pos_l(2) contains elevation
%  plane_wave:     plane wave primary source indicator (not for speakers)
%                     == 0: spherical wave (point source)
%                    =/= 0: plane wave
%  plot_area:      boundaries of the area where sound field is computed.
%                  format [x_min x_max y_min y_max]
%  delta:          x and y spacing of points on a grid where the field is
%                  computed
%  t:              time instant at which the field is computed
%  ldspk_filters:  array containing filters
%  ldspk_delays:   array containing initial delays for each loudspeaker [s]
%  stimulus:       waveform to be emitted by all loudspeakers
%  max_amp:        maximum amplitude to limit the excessive gains due to
%                  1/r when r is small
%
% Outputs
%  field_snapshot: sound pressure field values at time instant t

% Author:          Mihailo Kolundzija (mihailo.kolundzija@epfl.ch)
% Date:            April 2011

if isempty(max_amp)
  max_amp = 1/delta/20;
end

if isempty(stim)
  disp('Stimulus missing');
  return;
end

if isempty(plane_wave) || plane_wave == 0
  plane_wave = 0;
else
  plane_wave = 1;
end

x_range = plot_area(1):delta:plot_area(2);
y_range = plot_area(3):delta:plot_area(4);
Nx = length(x_range);
Ny = length(y_range);
N_l = size(pos_l,1);

max_delay = max(ldspk_delays);
% convert to the number of samples
max_delay = round(max_delay * f_s);
% matrix holding delayed stimuli
q = zeros(N_l, max(max_delay+length(stim),round(t*f_s)));
for i = 1:N_l
  % delay the stimulus by the delay of the i-th loudspeaker
  del = round(ldspk_delays(i) * f_s) + 1;
  q(i, del:del+length(stim)-1) = stim;
  % filter the delayed stimulus to obtain the loudspeaker output
  q(i,:) = fftfilt(ldspk_filters(i,:), q(i,:));
end

% compute the sound field snapshot

field_snapshot = zeros(Ny,Nx);
nx = 1;
ny = 1;
for x = x_range,
  for y = y_range,
    r_r = [x y];
    if plane_wave == 0 % for point sources
      for i = 1:N_l
        r_i = pos_l(i,:);
        r = sqrt(sum((r_r-r_i).^2));
        tau = r / c;
        r = max(r, 1/max_amp);
        % find the sample in the i-th loudspeaker output signal which
        % corresponds to its signal at time t in the point r_r
        sample_offset = round((t-tau)*f_s);
        if sample_offset <= 0
          sample_value = 0;
        else
          sample_value = q(i, sample_offset);
        end
        field_snapshot(ny,nx) = ...
          field_snapshot(ny,nx) + 1/(4*pi*r) .* sample_value;
      end
    else % for plane wave reproduction
      del = x/c * cos(pos_l(1,1)) * cos(pos_l(1,2)) + ...
            y/c * sin(pos_l(1,1)) * cos(pos_l(1,2));
      sample_offset = round((t - del) * f_s);
      if sample_offset <= 0
        sample_value = 0;
      else
        sample_value = stim(sample_offset);
      end
      field_snapshot(ny,nx) = field_snapshot(ny,nx) + sample_value;
    end
    ny = ny + 1;
  end
  nx = nx + 1;
  ny = 1;
end

field_snapshot = real(field_snapshot);
