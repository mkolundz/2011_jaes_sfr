function [spks, ctr_pts, spks_idxs] = spk_ctrl_select(r_m, ...
                                                      plane_wave, ...
                                                      pos_l, ...
                                                      pos_c, ...
                                                      margin_l, ...
                                                      margin_c)
% spk_ctrl_select - selects a sub-array of loudspeakers and a sub-array of
%                   control points based on the position of the primary
%                   source, positions of loudspeakers, and positions of
%                   control points.
%
% Inputs
%  r_m:        if plane_wave == 0,  r_m contains location of the primary
%                                   source (default 2D) 1x2
%              if plane_wave =/= 0, r_m(1) contains azimuth angle of
%                                   plane wave propagation;
%                                   r_m(2) contains elevation
%  plane_wave: plane wave primary source indicator
%                   0: spherical wave (point source)
%               =/= 0: plane wave
%  pos_l:      positions of loudspeakers in the array
%  pos_c:      positions of control points
%  margin_l:   margin (slack) to allow for the use of extra loudspeakers
%              (which might not be "visible")
%  margin_c:   margin (slack) to allow for the use of extra control points
%              (which are not "visible")
%
% Outputs
%  spks:       positions of selected loudspeakers
%  ctr_pts:    positions of selected control points
%  spks_idxs:  indexes of selected loudspeakers

% Author:      Mihailo Kolundzija (mihailo.kolundzija@epfl.ch)
% Date:        April 2011

N_c = size(pos_c,1);
N_l = size(pos_l,1);

if isempty(plane_wave) || plane_wave == 0
  plane_wave = 0;
else
  plane_wave = 1;
end

% determine the cone enclosing control points
if plane_wave == 0
  sin_ang_min = 1;
  sin_ang_max = -1;
  cos_ang_max = 0;
  cos_ang_min = 0;
  for i = 1:N_c
    sin_ang = ((pos_c(i,:)-r_m) * [0; 1]) ./ sqrt(sum((pos_c(i,:)-r_m).^2));
    cos_ang = ((pos_c(i,:)-r_m) * [1; 0]) ./ sqrt(sum((pos_c(i,:)-r_m).^2));
    if sin_ang > sin_ang_max
      sin_ang_max = sin_ang;
      cos_ang_max = cos_ang;
    end
    if sin_ang < sin_ang_min
      sin_ang_min = sin_ang;
      cos_ang_min = cos_ang;
    end
  end
else % plane wave primary source
  % cos and sin of the angle of plane wave propagation
  cos_ang = cos(r_m(1)) * cos(r_m(2));
  sin_ang = sin(r_m(1)) * cos(r_m(2));
  % if the wave propagates backward, invert the vector
  if cos_ang < 0
    cos_ang = -cos_ang;
    sin_ang = -sin_ang;
  end
  % find the control points which are on the boundary of the stripe
  % defined by the propagation vector
  if cos_ang ~= 0 % if the strip is not vertical
    max_k = pos_c(1,2) - pos_c(1,1) / cos_ang * sin_ang;
    min_k = max_k;
    max_pos = pos_c(1,:);
    min_pos = pos_c(1,:);
    for i = 2:N_c
      k = pos_c(i,2) - pos_c(i,1) / cos_ang * sin_ang;
      if k > max_k
        max_k = k;
        max_pos = pos_c(i,:);
      end
      if k < min_k
        min_k = k;
        min_pos = pos_c(i,:);
      end
    end
  else
    % if the stripe is vertical, just assume it's horizontal for
    % simplicity
    max_pos = pos_c(1,:);
    min_pos = pos_c(1,:);
    for i = 2:N_c
      if pos_c(i,2) > max_pos(2)
        max_pos = pos_c(i,:);
      end
      if pos_c(i,2) < min_pos(2)
        min_pos = pos_c(i,:);
      end
    end
  end
end

% determine the loudspeaker subset
if plane_wave == 0
  if r_m(1) <= pos_l(1,1) % primary source behind the array
    if cos_ang_max == 0 % avoid division by zero
      y_max = max(pos_l(:,2));
    else
      y_max = r_m(2) + (pos_l(1,1)-r_m(1)) .* sin_ang_max ./ cos_ang_max;
    end
    if cos_ang_min == 0 % avoid division by zero
      y_min = min(pos_l(:,2));
    else
      y_min = r_m(2) + (pos_l(1,1)-r_m(1)) .* sin_ang_min ./ cos_ang_min;
    end
  else % primary source in front of the array
    if cos_ang_max == 0 % avoid division by zero
      y_min = min(pos_l(:,2));
    else
      y_min = r_m(2) + (pos_l(1,1)-r_m(1)) .* sin_ang_max ./ cos_ang_max;
    end
    if cos_ang_min == 0 % avoid division by zero
      y_max = max(pos_l(:,2));
    else
      y_max = r_m(2) + (pos_l(1,1)-r_m(1)) .* sin_ang_min ./ cos_ang_min;
    end
  end
else % plane wave == 1
  if cos_ang == 0
    y_max = max(pos_c(:,2));
  else
    y_max = max_pos(2) - (max_pos(1)-pos_l(1,1)) / cos_ang * sin_ang;
  end
  if cos_ang == 0
    y_min = min(pos_c(:,2));
  else
    y_min = min_pos(2) - (min_pos(1)-pos_l(1,1)) / cos_ang * sin_ang;
  end
end

spks_idxs = (pos_l(:,2) <= y_max + margin_l) & ...
  (pos_l(:,2) >= y_min - margin_l);
spks = pos_l(spks_idxs, :);
spks_idxs = find(spks_idxs);

if isempty(spks)
  % one cannot reproduce a source with a given array - try with all
  % loudspeakers
  spks = pos_l;
  spks_idxs = 1:N_l;
end

% control points selection

if plane_wave == 0
  r_1 = zeros(1,3);
  r_2 = zeros(1,3);
  if r_m(1) < spks(1,1) % primary source behind the array
    % bottom margin of the enclosing cone
    r_1(1,1) = spks(1,1)-r_m(1);
    r_1(1,2) = min(spks(:,2))-r_m(2);
    % top margin of the enclosing cone
    r_2(1,1) = spks(1,1)-r_m(1);
    r_2(1,2) = max(spks(:,2))-r_m(2);
  else
    % bottom margin of the enclosing cone
    r_1(1,1) = r_m(1)-spks(1,1);
    r_1(1,2) = r_m(2)-max(spks(:,2));
    % top margin of the enclosing cone
    r_2(1,1) = r_m(1)-spks(1,1);
    r_2(1,2) = r_m(2)-min(spks(:,2));
  end

  idx = 0;
  ctr_pts_idxs = [];
  for i = 1:N_c
    r = zeros(1,3);
    r(1:2) = pos_c(i,1:2)-r_m;
    marg_c = [0 margin_c 0];
    if sum(cross(r_1, r+marg_c)) > 0 && sum(cross(r_2, r-marg_c)) < 0
      idx = idx + 1;
      ctr_pts_idxs(idx) = i;
    end
  end
else % plane_wave == 1
  r = [cos_ang sin_ang 0];
  r_1 = zeros(1,3);
  r_2 = zeros(1,3);
  marg_c = [0 margin_c 0];
  idx = 0;
  ctr_pts_idxs = [];
  for i = 1:N_c
    % vector from the loudspeaker array top
    r_1(1:2) = pos_c(i,:)-[pos_l(1,1) max(pos_l(:,2))];
    % vector from the loudspeaker array bottom
    r_2(1:2) = pos_c(i,:)-[pos_l(1,1) min(pos_l(:,2))];
    if sum(cross(r_1-marg_c, r)) > 0 && sum(cross(r_2+marg_c, r)) < 0
      idx = idx + 1;
      ctr_pts_idxs(idx) = i;
    end
  end
end
if isempty(ctr_pts_idxs)
  ctr_pts = pos_c;
else
  ctr_pts = pos_c(ctr_pts_idxs,:);
end

