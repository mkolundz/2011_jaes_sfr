function [win] = tapering_window(pos_l, window_type, trans_band_size)
% tapering_window - computes a tapering window that is used to avoid
%                   diffraction waves in WFS.
% Inputs
%  pos_l:           loudspeaker positions
%  window_type:     type of the used pruning window
%                    1: rectangular window
%                    2: sine window
%                    3: tapering window with a defined transition band
%  trans_band_size: length of the taper on both sides of tapering window
%                   used only in case window_type == 3
%
% Outputs
%  win:             tapering window to be used with the loudspeaker array

% Author:           Mihailo Kolundzija (mihailo.kolundzija@epfl.ch)
% Date:             April 2011

% compute the window
N_l = size(pos_l, 1);
x = 0.5:N_l-0.5;
if window_type == 1
    win = ones(1,N_l);
elseif window_type == 2
    win = sin(x./N_l.*pi);
elseif window_type == 3
    %trans_band_size = round(filter_size/8);
    xx = 1:trans_band_size;
    trans_band = 1/2 - 1/2 .* cos(xx./(trans_band_size+1)*pi);
    win = [trans_band ones(1,N_l-2*trans_band_size) fliplr(trans_band)];
end

