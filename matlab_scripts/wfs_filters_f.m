function [H_wfs wfs_delays] = wfs_filters_f(S_f, ...
                                            c, ...
                                            d_l, ...
                                            pos_l, ...
                                            pos_ref, ...
                                            r_m, ...
                                            plane_wave, ...
                                            wfs_win, ...
                                            norm_gains)
% wfs_filters_f - computes WFS filters for a given setup and line of control
%                 points at a given frequency S_f (frequency-domain filters)
% Inputs
%  S_f:        temporal frequency [Hz]
%  c:          speed of sound [m/s]
%  d_l:        distance between loudspeakers [m]
%  pos_l:      array containing locations of loudspeakers (default 2D) Nx2
%  pos_ref:    array containing locations of reference points
%              (default 2D) Nx2
%  r_m:        if plane_wave == 0,  r_m contains location of the primary
%                                   source (default 2D) 1x2
%              if plane_wave =/= 0, r_m(1) contains azimuth angle of
%                                   plane wave propagation;
%                                   r_m(2) contains elevation
%  plane_wave: plane wave primary source indicator
%                 == 0: spherical wave (point source)
%                =/= 0: plane wave
%  win:        tapering window applied to the array (default rectangular)
%  norm_gains: control parameter for normalizing loudspeaker gains
%                0: don't fix gains
%                1: normalize for all control points (default)
%                2: normalize for central control point
%
% Outputs
%  H_wfs:      complex loudspeaker filter values
%  wfs_delays: delays associated to loudspeakers

% Author:      Mihailo Kolundzija (mihailo.kolundzija@epfl.ch)
% Date:        April 2011

if nargin == 6
  % by default, use rectangular (no) tapering
  wfs_win = ones(size(pos_l,1),1);
  norm_gains = 1; % by default, normalize power on the line
end

if nargin == 7
  norm_gains = 1; % by default, normalize power on the line
end

if isempty(plane_wave) || plane_wave == 0
  plane_wave = 0;
else
  plane_wave = 1;
end

k = 2*pi*S_f/c; % wave number
N_wfs = size(pos_l,1); % number of loudspeakers
deltai_wfs = d_l; % inter-loudspeaker distance
x_l = pos_l(1,1); % x coordinate of the loudspeaker line
x_c = pos_ref(1,1); % x coordinate of the control line
wfs_delays = zeros(N_wfs,1);
H_wfs = zeros(N_wfs,1);

% compute WFS filters
if plane_wave == 0 %(point source reproduction)
  for i = 1:N_wfs,
    r_i = pos_l(i,:);
    % compute a_i
    r = norm(r_i-r_m);
    cos_phi = abs(x_l-r_m(1))/r;
    % approximation on line with x = x_l
    a_i = sqrt(k/2/pi) * sqrt(abs(x_c-x_l)/abs(x_c-r_m(1))) * ...
          cos_phi / sqrt(r) * deltai_wfs;
    tau_i = (r) / c;
    % modification for source in front of array
    if r_m(1) > x_l
      tau_i = -tau_i;
    end
    wfs_delays(i) = tau_i;
    H_wfs(i) = a_i .* exp(-1j*2*pi*S_f*tau_i) .* exp(1j*pi/4);
    H_wfs(i) = H_wfs(i) .* wfs_win(i);
  end
else % plane wave reproduction
  for i = 1:N_wfs,
    H_wfs(i) = sqrt(8*pi*abs(x_c-x_l)) * sqrt(k) * sqrt(cos(r_m(1))) * ...
               deltai_wfs * ... % amplitude part
               exp(1j*pi/4) * ...
               exp(-1j * k * (pos_l(i,1)*cos(r_m(1))*cos(r_m(2)) + ...
                              pos_l(i,2)*sin(r_m(1))*cos(r_m(2))));
    wfs_delays(i) = (pos_l(i,1)*cos(r_m(1))*cos(r_m(2)) + ...
                     pos_l(i,2)*sin(r_m(1))*cos(r_m(2))) / c;
    H_wfs(i) = H_wfs(i) .* wfs_win(i);
  end
end

% normalize gains to have a desired power on the reference line (1) or in
% the reference point (2)

if norm_gains == 1 && S_f > 0
  % normalize on the reference line (multiple points)
  % transfer matrix
  G_wfs = acoustic_transfer_matrix(S_f, c, pos_l, pos_ref, 0);
  % responses of WFS in control points
  A_wfs = G_wfs * H_wfs;
  % desired response in control points
  A = acoustic_transfer_matrix(S_f, c, r_m, pos_ref, plane_wave);
  avg_gain = sum(abs(A).^2)/length(A);
  avg_wfs_gain = sum(abs(A_wfs).^2)/length(A_wfs);
  H_wfs = H_wfs .* sqrt(avg_gain / avg_wfs_gain);
elseif norm_gains == 2 && S_f > 0
  % normalize in a single point
  % reference point's index in the array pos_ref (mid point)
  ref_idx = round(size(pos_ref,1)/2);
  % transfer matrix to reference point
  G_wfs = acoustic_transfer_matrix(S_f, c, pos_l, pos_ref(ref_idx,:));
  % responses of WFS in control points
  A_wfs = G_wfs * H_wfs;
  A = acoustic_transfer_matrix(S_f, c, r_m, ...
    pos_ref(ref_idx,:), plane_wave);
  avg_gain = sum(abs(A).^2)/length(A);
  avg_wfs_gain = sum(abs(A_wfs).^2)/length(A_wfs);
  H_wfs = H_wfs .* sqrt(avg_gain / avg_wfs_gain);
end
