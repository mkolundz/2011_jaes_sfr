function [filters, delays, neg_delay] = wfs_filters_t(fft_size, ...
                                                      filter_size, ...
                                                      f_s, ...
                                                      c, ...
                                                      d_l, ...
                                                      pos_l, ...
                                                      pos_ref, ...
                                                      r_m, ...
                                                      plane_wave, ...
                                                      wfs_win, ...
                                                      norm_gains, ...
                                                      lp_filter, ...
                                                      prun_win)
% wfs_filters_t - computes WFS filters for a given setup and line of
%                 reference points for which the amplitude is normalized.
%
% Inputs
%  fft_size:     size of the DFT used for filter computation
%  filter_size:  length of loudspeaker filters [samples]
%  f_s:          sampling frequency [Hz]
%  c:            speed of sound [m/s]
%  d_l:          distance between loudspeakers [m]
%  pos_l:        array of locations of loudspeakers (default 2D) Nx2
%  pos_ref:      array of locations of reference points (default 2D) Nx2
%  r_m:          if plane_wave == 0,  r_m contains location of the primary
%                                     source (default 2D) 1x2
%                if plane_wave =/= 0, r_m(1) contains azimuth angle of
%                                     plane wave propagation;
%                                     r_m(2) contains elevation
%  plane_wave:   plane wave primary source indicator
%                   == 0: spherical wave (point source)
%                  =/= 0: plane wave
%  win:          tapering window applied to the array (default rectangular)
%  norm_gains:   control parameter for normalizing loudspeaker gains
%                  0: don't fix gains
%                  1: normalize for all control points (default)
%                  2: normalize for central control point
%  lp_filter:    DFT of a low pass filter for cutting off freqs > 20 kHz
%  prun_win:     window for pruning the loudspeaker impulse responses
%
% Outputs
%  filters:      loudspeaker filters
%  delays:       delays associated to loudspeakers
%  neg_delay:    possible delay that needs to be subtracted to make
%                delays non-negative

% Author:        Mihailo Kolundzija (mihailo.kolundzija@epfl.ch)
% Date:          April 2011

% call wfs_filters_f for all frequencies in S_F
% extract delays and treat them separately

N_wfs = size(pos_l,1); % number of loudspeakers

% size of a single FFT bin
fft_bin_size = f_s/fft_size;
% primary source's frequencies for computing filters
S_F = 0:fft_bin_size:(fft_size/2)*fft_bin_size;

% initialize to the default value 0
neg_delay = 0;

% for each frequency in S_F compute complex filters' response for all
% loudspeakers

i = 0;
H_wfs_f = zeros(size(pos_l,1),length(S_F));
for S_f = S_F
  i = i+1;
  [H_wfs_f(:,i), delays] = ...
    wfs_filters_f(S_f, c, d_l, pos_l, pos_ref, ...
                  r_m, plane_wave, wfs_win, norm_gains);
end
% H_wfs_f contains frequency-domain WFS filters

% make a multiple of the sampling interval
% wfs_delays = floor(wfs_delays*f_s)/f_s;
wfs_delay_compensation = delays - fft_size/(2*f_s);
for i = 1:N_wfs
  H_wfs_f(i,:) = H_wfs_f(i,:) .* ...
    exp(1j*S_F*2*pi*wfs_delay_compensation(i));
end

% make sure delays are non-negative
if min(delays) < 0 % there are negative delays - focused source
  neg_delay = - min(delays);
  delays = delays - min(delays);
end

% apply low-pass filter

% make sure it is a row vector
lp_filter = lp_filter(:)';
% assure it has same length as H_sfr_f
lp_filter = lp_filter(1:length(S_F));
H_wfs_f = H_wfs_f .* kron(lp_filter, ones(N_wfs,1));

% make the response of WFS filters real (make spectrum conjugate symmetric)
H_wfs_real = [H_wfs_f conj(fliplr(H_wfs_f(:,2:end-1)))];
H_wfs_time = real(ifft(H_wfs_real.').');

% prune (window) the obtained time-domain filters
window = prun_win(:)'; % make sure it is a row vector
H_wfs_time_w = H_wfs_time .* kron(window, ones(size(H_wfs_time,1),1));

% indexes of the filters' impulse responses to be preserved
filter_idx = fft_size/2-filter_size/2+1:fft_size/2+filter_size/2;
% extract the short filters
filters = H_wfs_time_w(:,filter_idx);
