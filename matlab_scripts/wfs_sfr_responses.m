function [des_rsps, wfs_rsps, sfr_rsps] = wfs_sfr_responses(f_s, ...
                                                            filt_size, ...
                                                            c, ...
                                                            r_m, ...
                                                            pos_wfs, ...
                                                            pos_sfr, ...
                                                            pos_c, ...
                                                            stimulus, ...
                                                            wfs_filters, ...
                                                            wfs_delays, ...
                                                            sfr_filters, ...
                                                            sfr_delays, ...
                                                            norm_type)
% wfs_sfr_responses - computes responses to a stimulus of a desired source,
%                     WFS and SFR setups in a set of control points pos_c
%
% Inputs
%  f_s:           sampling frequency [Hz]
%  filt_size:     length of SFR and WFS filters
%  c:             speed of sound [m/s]
%  r_m:           position of the primary source
%  pos_wfs:       locations of WFS loudspeakers
%  pos_sfr:       locations of SFR loudspeakers
%  pos_c:         locations of control (listening) points
%  stimulus:      excitation waveform
%  wfs_filters:   a set of time-domain filters for WFS
%  wfs_delays:    a set of initial delays for WFS [s]
%  sfr_filters:   a set of time-domain filters for SFR
%  sfr_delays:    a set of initial delays for SFR [s]
%  norm_type:     normalization selection for final resps
%                  1: normalize output to have the same energy as the input
%                  2: normalize to have the same maximum amplitude (avoids
%                     clipping)
% Outputs
%  des_rsps:      responses for a desired source in locations given by pos_c
%  wfs_rsps:      responses of a WFS setup in locations given by pos_c
%  sfr_rsps:      responses of s SFR setup in locations given by pos_c

% Author:         Mihailo Kolundzija (mihailo.kolundzija@epfl.ch)
% Date:           April 2011

N_c = size(pos_c, 1);
N_sfr = size(pos_sfr, 1);
N_wfs = size(pos_wfs, 1);

% maximum delay of all (SFR and WFS) filters
max_filt_delay = max(max(sfr_delays), max(wfs_delays));
% minimum delay of all (SFR and WFS) filters
min_filt_delay = min(min(sfr_delays), min(wfs_delays));

% compute matrix with distances between SFR loudspeakers and
% listening points
r_sfr = zeros(N_c,N_sfr);
for i = 1:size(pos_sfr,2)
  M = ones(N_c,1) * pos_sfr(:,i)';
  N = pos_c(:,i) * ones(1,N_sfr);
  r_sfr = r_sfr + (M-N).^2;
end
r_sfr = sqrt(r_sfr);

% compute matrix with distances between WFS loudspeakers and
% listening points
r_wfs = zeros(N_c,N_wfs);
for i = 1:size(pos_wfs,2)
  M = ones(N_c,1) * pos_wfs(:,i)';
  N = pos_c(:,i) * ones(1,N_wfs);
  r_wfs = r_wfs + (M-N).^2;
end
r_wfs = sqrt(r_wfs);

prop_delay_sfr = r_sfr ./ c;
prop_delay_wfs = r_wfs ./ c;
max_prop_delay = max(max(max(prop_delay_sfr)),max(max(prop_delay_wfs)));
min_prop_delay = min(min(min(prop_delay_sfr)),min(min(prop_delay_wfs)));

% min and max delays (including propagation and filtering delays)
min_delay = min_prop_delay + min_filt_delay;
max_delay = max_prop_delay + max_filt_delay;
% accomodate for the maximum delay, but shift left by min_delay samples
% (leading zeros not important)
common_delay = max_delay - min_delay;
common_delay_int = ceil(common_delay .* f_s);

sfr_rsps = zeros(N_c, ...
                 common_delay_int + size(stimulus,2) + size(sfr_filters,2));
wfs_rsps = zeros(N_c, ...
                 common_delay_int + size(stimulus,2) + size(wfs_filters,2));
% compute responses in all listening points
for i = 1:N_c
  % stimulus will include the leading (delay) zeros, the stimulus signal,
  % and trailing zeros so that the convolution is not truncated
  stimulus_sfr_delayed = ...
    zeros(size(sfr_filters,1), ...
          common_delay_int + size(stimulus,2) + size(sfr_filters,2));
  for ii = 1:size(sfr_filters,1)
    % propagation and filtering delay
    filt_and_prop_delay = ...
      floor((prop_delay_sfr(i,ii) + sfr_delays(ii) - min_delay) .* f_s);
    stimulus_sfr_delayed(ii,1:filt_and_prop_delay+size(stimulus,2)) = ...
      [zeros(1,filt_and_prop_delay) stimulus(1,:)];
    % compute the attenuation due to propagation (1/r)
    prop_attenuation = 1 ./ r_sfr(i,ii);
    sfr_rsps(i,:) = prop_attenuation .* ...
                    fftfilt(sfr_filters(ii,:),stimulus_sfr_delayed(ii,:)) + ...
                    sfr_rsps(i,:);
  end

  stimulus_wfs_delayed = ...
    zeros(size(wfs_filters,1), ...
          common_delay_int + size(stimulus,2) + size(wfs_filters,2));
  for ii = 1:size(wfs_filters,1)
    % propagation and filtering delay
    filt_and_prop_delay = ...
      floor((prop_delay_wfs(i,ii) + wfs_delays(ii) - min_delay) .* f_s);
    stimulus_wfs_delayed(ii,1:filt_and_prop_delay+size(stimulus,2)) = ...
      [zeros(1,filt_and_prop_delay) stimulus(1,:)];
    % compute the attenuation due to propagation (1/r)
    prop_attenuation =  1 ./ r_wfs(i,ii);
    wfs_rsps(i,:) = prop_attenuation .* ...
                    fftfilt(wfs_filters(ii,:),stimulus_wfs_delayed(ii,:)) + ...
                    wfs_rsps(i,:);
  end
end
% compute desired responses in listening points
% distance from the primary source to listening positions
r_m_l = zeros(N_c,1);
for i = 1:size(r_m,2)
  M = ones(N_c,1) * r_m(:,i)';
  N = pos_c(:,i) * ones(1,1);
  r_m_l = r_m_l + (M-N).^2;
end
r_m_l = sqrt(r_m_l);
prop_delay_m = r_m_l ./ c + filt_size/2/f_s;
prop_attenuation = 1./r_m_l;
des_rsps = zeros(N_c, ...
  common_delay_int + size(stimulus,2) + size(sfr_filters,2));
for i = 1:N_c
  stimulus_m_delayed = zeros(1, ...
    common_delay_int + size(stimulus,2) + size(sfr_filters,2));
  corrected_delay = floor((prop_delay_m(i)-min_delay).*f_s);
  stimulus_m_delayed(1:corrected_delay+size(stimulus,2)) = ...
    [zeros(1,corrected_delay) stimulus(1,:)];
  des_rsps(i,:) = prop_attenuation(i) .* stimulus_m_delayed;
end

if norm_type == 1
  % normalize outputs so that the maximum of all has equal or lower energy
  % as the input (make sense for listening)
  max_sfr_en = max(sum(sfr_rsps.^2,2));
  max_wfs_en = max(sum(wfs_rsps.^2,2));
  max_desired_en = max(sum(des_rsps.^2, 2));
  max_en = max(max(max_desired_en, max_wfs_en), max_sfr_en);
  input_en = sum(stimulus.^2);
  if max_en > input_en
    wfs_rsps = wfs_rsps .* sqrt(input_en / max_en);
    des_rsps = des_rsps .* sqrt(input_en / max_en);
    sfr_rsps = sfr_rsps .* sqrt(input_en / max_en);
  end
elseif norm_type == 2
  % normalize outputs so that the maximum of all has the same or lower
  % maximum amplitude as the input (avoid clipping)
  max_wfs_en = max(abs(wfs_rsps(:)));
  max_desired_en = max(abs(des_rsps(:)));
  max_sfr_en = max(abs(sfr_rsps(:)));
  max_en = max(max(max_desired_en,max_sfr_en), max_wfs_en);
  input_en = max(abs(stimulus(:)));
  if max_en > input_en
    wfs_rsps = wfs_rsps .* (input_en / max_en);
    des_rsps = des_rsps .* (input_en / max_en);
    sfr_rsps = sfr_rsps .* (input_en / max_en);
  end
end
