function y = zero_phase_mov_avg(x, win_len)
% zero_phase_moveavg - computes zero-mean moving average of x column-wise
%                      using an averaging window of a given length
%
% Inputs
%   x:        input sequence
%   win_len:  length of the moving average filter (will be odd)
%
% Outputs
%   y:        smoothed sequence x

% Author:     Mihailo Kolundzija (mihailo.kolundzija@epfl.ch)
% Date:       November 2014

% make sure window length is odd
if mod(win_len, 2) == 0
  win_len = win_len + 1;
end

% moving average filter of length win_len
avg_filter = 1/win_len * ones(win_len, 1);

% if x is a vector, make it a column vector
if min(size(x)) == 1
  y = x(:);
else
  y = x;
end

m_x = size(y, 1);
n_x = size(y, 2);

% zero-pad x with (win_len-1) zeros
y = [y; zeros(win_len-1, n_x)];

y = fftfilt(avg_filter, y);

% extract the mid-part of y to make the result zero-phase
delay = (win_len - 1) / 2;
y = y((delay+1):(delay+m_x), :);

% transpose y if x has been previously transposed
if ~all(size(x) == size(y))
  y = y.';
end
